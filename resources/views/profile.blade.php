@extends('layouts.master')

@section('nav-home')
    active
@endsection

@section('tittle')
    Profile
@endsection

@section('search')
<!-- <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form> -->
@endsection

@section('content')
<!-- Header -->
<!-- <div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-image: url(/assets/img/theme/profile-cover2.jpg); background-size: cover; background-position: center top;"> -->
    <!-- <span class="mask bg-gradient-default opacity-8"></span> -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-12 col-12">
                    @if ($message = Session::get('gagal'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @elseif ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @endif
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Profile</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<style>
    .card{
        margin-bottom:10px;
    }

</style>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-4 col-md-4 order-xl-2 order-md-2">
            <div class="card card-profile">
                <img src="/assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                        <a href="{{ route('home') }}">
                            <img src="/assets/img/brand/brand.jpg" class="rounded-circle">
                        </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <!-- <div class="d-flex justify-content-between">
                    <a href="#" class="btn btn-sm btn-info  mr-4 ">Connect</a>
                    <a href="#" class="btn btn-sm btn-default float-right">Message</a>
                </div> -->
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col">
                        <div class="card-profile-stats d-flex justify-content-center">
                            <!-- <div>
                            <span class="heading">22</span>
                            <span class="description">Friends</span>
                            </div>
                            <div>
                            <span class="heading">10</span>
                            <span class="description">Photos</span>
                            </div>
                            <div>
                            <span class="heading">89</span>
                            <span class="description">Comments</span>
                            </div> -->
                        </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h5 class="h3">
                        {{strtoupper(Auth::user()->name)}}<span class="font-weight-light">, {{Auth::user()->id}} </span>
                        </h5>
                        {{-- <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>@ipm_jatim
                        </div> --}}
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>Admin e-Vote SMKM 1 Kertosono
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>Ikatan Pelajar Muhammadiyah <br> SMKM 1 Kertosono
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-md-8 order-xl-1 order-md-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <h3 class="mb-0">Edit profile </h3>
                        </div>
                        <div class="col-6 text-right">
                            <!-- <span>
                                <a href="#!" class="btn btn-sm btn-primary">Ubah Profil</a>
                            </span> -->
                            <!-- <a href="#!" class="btn btn-sm btn-danger">Ubah Password</a> -->
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="/admin/profile/{{Auth::user()->id}}/update">
                        @csrf
                        <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                        <div class="pl-lg">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Nama</label>
                                        <input type="text" name="name" class="form-control" value="{{strtoupper(Auth::user()->name)}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Alamat Email</label>
                                        <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <a href="" data-toggle="modal" data-target="#modal-ganti-password" class="btn btn-sm btn-danger">Ubah Password</a>
                                </div>
                                <div class="col-6 text-right">
                                    <span>
                                        <button type="submit" class="btn btn-sm btn-info">Simpan Perubahan</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="modal-ganti-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" action="{{route('ganti-password')}}">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Password Lama</label>
                                    <input type="password" name="password_lama" class="form-control"placeholder="Masukkan Password Lama">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Password Baru</label>
                                    <input type="password" name="password" class="form-control"placeholder="Masukkan Password Baru">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
