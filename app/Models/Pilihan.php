<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pilihan extends Model
{
    use HasFactory;
    protected $table = 'pilihan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'token',
        'id_kegiatan',
        'pilihan',
    ];
}
