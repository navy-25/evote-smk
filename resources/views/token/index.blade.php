@extends('layouts.master')

@section('nav-token')
    active
@endsection

@section('tittle')
    Kelola Token
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kelola Token</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <a href="{{route('token-cetak')}}" target="_blank" title="Cetak token" class="btn btn-sm btn-neutral"><i class="ni ni-folder-17" style="margin-right:7px"></i> Print</a> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <?php
            $sudah_digunakan = 0;
            $belum_digunakan = 0;
            for($i=0;$i<count($token);$i++){
                if($token[$i]->status == '1'){
                    $belum_digunakan =  $belum_digunakan + 1;
                }else{
                    $sudah_digunakan =  $sudah_digunakan + 1;
                }
            }
        ?>
        <div class="col" style="margin-bottom:10px">
            <button class="btn btn-sm btn-success">Token kosong : {{$belum_digunakan}} token</button>
            <button class="btn btn-sm btn-danger">Token Hangus : {{$sudah_digunakan}} token</button>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">
                                Data Token
                            </h3>
                        </div>
                        <div class="col text-right">
                            <button data-toggle="modal" data-target="#modal-token" title="Buat token baru"  class="btn btn-sm btn-primary">+ Buat Token</button>
                            <button onclick="exportTableToExcel('tblData', 'data token')" title="Export calon formatur" class="btn btn-sm btn-warning">Export Excel</button>
                            <button data-toggle="modal" data-target="#modal-settings" title="Kelola token"  class="btn btn-sm btn-info">
                                <span class="btn-inner--icon">
                                    <i class="ni ni-settings-gear-65"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush" id="tblData">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:25%">Kode Token</th>
                                <th scope="col" style="width:45%">Status</th>
                                <th scope="col" style="width:25%">Dibuat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($token as $x)
                                <tr>
                                    <th scope="row">{{$no++}}</th>
                                    <td>{{$x->name}}</td>
                                    <td>
                                        @if($x->status == 1)
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-success"></i>
                                            <span class="status">Siap Dipakai</span>
                                        </span>
                                        @else
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-danger"></i>
                                            <span class="status">Sudah Dipakai</span>
                                        </span>
                                        @endif
                                    </td>
                                    <td>{{$x->updated_at}}</td>
                                </tr>
                            @endforeach
                            @if(count($token) == 0)
                                <tr>
                                    <th colspan="4">
                                        <center>Tidak ada data</center>
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="modal-token" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" action="{{route('token-store')}}">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Jumlah Token</label>
                                    <input type="number" name="jumlah" class="form-control" placeholder="Masukkan jumlah token yang akan dibuat">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-settings" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>
                <span class="btn-inner--icon">
                    <i class="ni ni-settings-gear-65" style="oadding-bottom:10px;margin-right:10px"></i>Setting</h2>
                </span>
            </div>
            <div class="modal-body" style="padding-top:0px">
                <div class="row">
                    @if($sudah_digunakan == 0)
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding:5px">
                        @if(count($token) != 0)
                            @if($token[0]->status == 1)
                                <a href="/admin/token/status"  title="Non Aktifkan semua token" style="width:100%" class="btn btn-info nonaktif-confirm">x Non Aktifkan semua token</a>
                            @elseif($token[0]->status == 0)
                                <a href="/admin/token/status" style="width:100%"   title="Aktifkan semua token" class="btn btn-success aktif-confirm"><i class="ni ni-check-bold" style="margin-right:7px"></i>Aktifkan semua token</a>
                            @endif
                        @endif
                    </div>
                    @endif
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding:5px">
                        <a href="{{route('token-cetak')}}" target="_blank"  style="width:100%"  title="Cetak token" class="btn btn-warning"><i class="ni ni-folder-17" style="margin-right:7px"></i>Download / Cetak</a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding:5px">
                        <a href="{{route('token-delete')}}"  title="Hapus semua token"  style="width:100%" class="btn btn-danger delete-confirm"><i class="ni ni-basket" style="margin-right:7px"></i>Hapus Semua</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin dihapus ?',
            text: 'Jangan hapus token ketika pemilihan sedang berlangsung !',
            icon: 'warning',
            buttons: ["Batalkan", "Hapus"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });

    $('.nonaktif-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Nonaktifkan semua token ?',
            text: 'Jangan non aktifkan token ketika pemilihan sedang berlangsung !',
            icon: 'warning',
            buttons: ["Batalkan", "Nonaktif"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });

    $('.aktif-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Aktifkan semua token ?',
            text: 'Anda bisa menonaktifkan kembali token yang sudah aktif',
            icon: 'warning',
            buttons: ["Batalkan", "Aktifkan"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection
