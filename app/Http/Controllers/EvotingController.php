<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
// use Carbon\Carbon;

class EvotingController extends Controller
{
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        $history = \App\Models\History::orderby('id','DESC')->get();
        $history_today = \App\Models\History::all();
        for($i=0 ;$i<count($history_today);$i++){
            if($history_today[$i]->status == 'start'){
                return redirect('/admin/evoting/'.$history_today[$i]->id.'/timer');
                break;
            }
        }
        return view('evoting.index',compact('history'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $all_history = \App\Models\History::all();
        for($i=0 ;$i<count($all_history);$i++){
            if($all_history[$i]->status == 'start'){
                return redirect('/admin/evoting/')->with(['gagal' => 'Gagal ! tidak boleh membuat kegiatan baru ketika proses voting berlangsung !!']);
            }
        }
        if( $request->name == null ||  $request->tanggal == null || $request->waktu_mulai == null ||  $request->waktu_akhir == null){
            return redirect('/admin/evoting/')->with(['gagal' => 'Gagal ! data tidak boleh kosong']);
        }
        try{
            $history = \App\Models\History::create([        
                'name' => $request->name,
                'tanggal' => $request->tanggal,
                'status' => "",
                'waktu_mulai' => $request->waktu_mulai,
                'waktu_akhir' => $request->waktu_akhir,
            ]);

            // mengecheck status voting
            $history_today = $history;
            $date_today = date('F d, Y');
            $time_today = date('H:i');
            $data_pemilihan = date_format(date_create($history_today->tanggal),'F d, Y');
            
            $waktu_today = explode(':',$time_today);
            $waktu_mulai = explode(':',$history_today->waktu_mulai);
            $waktu_akhir = explode(':',$history_today->waktu_akhir);
            $waktu_today_to_menit = (int)$waktu_today[0]*60 + $waktu_today[1];
            $waktu_mulai_to_menit = (int)$waktu_mulai[0]*60 + $waktu_mulai[1];
            $waktu_akhir_to_menit = (int)$waktu_akhir[0]*60 + $waktu_akhir[1];
            
            if($date_today == $data_pemilihan and $waktu_today_to_menit < $waktu_mulai_to_menit){
                $status = "pending";
            }elseif($date_today == $data_pemilihan and $waktu_today_to_menit >= $waktu_mulai_to_menit and $waktu_today_to_menit < $waktu_akhir_to_menit){
                $status = "start";
            }elseif($date_today == $data_pemilihan and $waktu_today_to_menit >= $waktu_akhir_to_menit){
                $status = "stop";
            }else{
                $status = "expired";
            }
            // update status
            $data = [            
                'status' => $status,
            ];
            $history->update($data);
            return redirect('/admin/evoting/')->with(['success' => 'Sukses mengatur pemilihan pada kegiatan '.$history->name]);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting/')->with(['gagal' => 'Sukses mengatur pemilihan pada kegiatan '.$history->nameh]);
        }
    }
    public function timer($id){
        date_default_timezone_set('Asia/Jakarta');
        $history = \App\Models\History::find($id);
        return view('evoting.timer',compact('history'));
    }
    public function destroy($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $history = \App\Models\History::find($id);
            $pilihan = \App\Models\Pilihan::where('id_kegiatan',$history->id)->count();
            if($pilihan > 0){
                $data = \App\Models\Pilihan::where('id_kegiatan',$history->id)->get();
                foreach($data as $x){
                    $del = \App\Models\Pilihan::find($data->id);
                    $del->delete($del);
                }
            }
            $history->delete($history);
            return redirect('/admin/evoting/')->with(['success' => $history->name.' berhasil dihapus']);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting')->with(['gagal' => $history->name.' gagal dihapus']);
        }
    }
    public function start($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $all_history = \App\Models\History::all();
            for($i=0 ;$i<count($all_history);$i++){
                if($all_history[$i]->status == 'start'){
                    return redirect('/admin/evoting/')->with(['gagal' => 'Gagal ! tidak memulai voting baru ketika proses voting lama sedang berlangsung !!']);
                }
            }
            $history = \App\Models\History::find($id);
            $data = [            
                'status' => "start",
            ];
            $massage = "Pemilihan sudah dibuka";
            $history->update($data);
            return redirect('/admin/evoting/'.$id.'/timer')->with(['success' => $massage]);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting')->with(['gagal' => $massage]);
        }
    }
    public function stop($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $history = \App\Models\History::find($id);
            $data = [            
                'status' => "stop",
            ];
            $massage = "Pemilihan sudah selesai, klik menu statistik untuk melihat hasil suara yang masuk";
            $history->update($data);
            return redirect('/admin/evoting/')->with(['success' => $massage]);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting')->with(['gagal' => $massage]);
        }
    }

    public function barchart($id)
    {
        try{
            $pilihan = \App\Models\Pilihan::where('id_kegiatan',$id)->get();
            if(count($pilihan) == 0){
                return redirect('/admin/evoting')->with(['gagal' => "Tidak bisa menampilkan grafik, karena hasil voting tidak ada !, lakukan voting untuk mengetahui hasil grafiknya."]);
            }
            $formatur = \App\Models\Formatur::all();
            $pilihan_all = [];
            
            for($i=0;$i<count($pilihan);$i++){
                $pilihan_all[$i] = $pilihan[$i]->pilihan;
            }
            sort($pilihan_all);
            $hasil_suara = array_count_values($pilihan_all);
    
            $suara = [];
            $label = [];
            foreach ($hasil_suara as $key => $value) {
                $formatur = \App\Models\Formatur::find($key);
                $nama = $formatur->name;
                $nama = explode(" ",$nama);
                array_push($label, $nama[0]);
                array_push($suara,$value);
            }
            $formatur_all = \App\Models\Formatur::all();
            return view('evoting.finish',compact('suara','label','hasil_suara','formatur_all','id'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(['gagal' => "Tidak bisa menampilkan grafik, karena hasil voting tidak ada !, lakukan voting untuk mengetahui hasil grafiknya."]);
        }
    }
    public function print($id)
    {
        $pilihan = \App\Models\Pilihan::where('id_kegiatan',$id)->get();
        if(count($pilihan) == 0){
            return redirect('/admin/evoting')->with(['gagal' => "Tidak bisa menampilkan grafik, karena hasil voting tidak ada !, lakukan voting untuk mengetahui hasil grafiknya."]);
        }
        $formatur = \App\Models\Formatur::all();
        $pilihan_all = [];
        
        for($i=0;$i<count($pilihan);$i++){
            $pilihan_all[$i] = $pilihan[$i]->pilihan;
        }
        sort($pilihan_all);
        $hasil_suara = array_count_values($pilihan_all);

        
        $formatur_all = \App\Models\Formatur::all();
        $pdf = PDF::loadview('evoting.print',['id'=>$id,'hasil_suara'=>$hasil_suara,'formatur_all'=>$formatur_all])->setPaper('A4','potrait');
        return $pdf->stream();
    }
    public function aktifkan_hasil()
    {
        $akses_hasil = \App\Models\History_Access::find(1);
        try{
            if($akses_hasil->status_akses == 1){
                $data = [            
                    'status_akses' => 0,
                ];
                $massage = "Hasil suara berhasil ditampilkan !";
            }else if($akses_hasil->status_akses == 0){
                $data = [            
                    'status_akses' => 1,
                ];
                $massage = "Hasil suara berhasil ditampilkan !";
            }
            $akses_hasil->update($data);
            return redirect('/admin/evoting/')->with(['success' => $massage]);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting')->with(['gagal' => "Hasil suara gagal ditampilkan !"]);
        }
    }
    public function batasi_formatur(Request $request)
    {
        $akses_hasil = \App\Models\History_Access::find(1);
        try{
            $data = [            
                'max_data_hasil' => $request->batas_formatur,
            ];
            $massage = "Batas berhasil di setel !";
            $akses_hasil->update($data);
            return redirect('/admin/evoting/')->with(['success' => $massage]);
        } catch (\Throwable $th) {
            return redirect('/admin/evoting')->with(['gagal' => "Hasil suara gagal ditampilkan !"]);
        }
    }
}
