<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Aplikasi e-Voting PR IPM SMKM 1 Kts. Dibuat untuk keperluan khusus musyawarah tingkat daerah ikatan pelajar muhammadiyah SMKM 1 Kertosono.">
  <meta name="author" content="IPR IPM SMKM 1 Kts">
  <title>e-Vote Jatim | Login Admin</title>
  <!-- Favicon -->
  <link rel="icon" href="/assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="/assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="/assets/css/argon.css?v=1.2.0" type="text/css">
</head>
<style>
    .bg-default{
        background-image: url("../assets/landing/images/bg-login-token.png");
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
</style>
<body class="bg-default">
   <!-- Main content -->
  <div class="main-content">
    <!-- Header -->
    <div class="header py-9 py-lg-8 pt-lg-4">
      <div class="container">
        <div class="header-body text-center mb-7">
            <img src="../assets/img/brand/white.png" width="200px">
        </div>
      </div>

    </div>
    <style>
      .mb-4, .my-4, .form-group {
        margin-bottom: 1rem !important;
      }
      .card-body {
        padding: 0.5rem;
      }
      .mb-7, .my-7 {
        margin-bottom: 3rem !important;
      }
    </style>
    <!-- Page content -->
    <div class="container mt--8">
      <div class="row justify-content-center">
        <div class="col-lg-4 col-md-6">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                <div class="form-group mb-3" >
                    <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                        </div>
                        <input id="name" type="text" placeholder="Nama Lengkap"  class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group mb-3" >
                    <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                        </div>
                        <input id="email" type="email" placeholder="Email"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group mb-3" >
                    <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                        </div>
                        <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group mb-3" >
                    <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                        </div>
                        <input id="password-confirm" type="password" placeholder="Password Confirm"  class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <button type="submit" style="width:100%" class="btn btn-primary my-4">Register</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="" id="footer-main">
    <div class="container">
        @include('includes.footer')
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="/assets/js/argon.js?v=1.2.0"></script>
</body>

</html>
