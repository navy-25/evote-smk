@extends('layouts.master')

@section('nav-evoting')
    active
@endsection

@section('tittle')
    Timer Voting {{$history->name}}
@endsection

@section('search')

@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card" >
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><span class="btn-inner--icon"><i class="ni ni-time-alarm" style="margin-right:10px;"></i></span>Timer Voting | {{$history->name}}</h3>
                        </div>
                        <div class="col text-right">
                            <!-- <a class="btn btn-sm btn-primary" href="/admin/evoting/">
                            <span class="btn-inner--icon">
                                <i class="ni ni-bold-left" style="margin-right:10px"></i>
                            </span>
                            Kembali</a> -->
                            <button class="btn btn-sm btn-primary" onclick="openFullscreen();">
                            
                            Fullscreen</button>
                        </div>
                        
                    </div>
                </div>
                <div class="card-header border-0" id="fullscreen" >
                    <div class="row align-items-center">
                        
                    </div>
                    <?php
                        $date =  $history->tanggal." ".$history->waktu_akhir.':00';                        
                    ?>
                    @if($history->status == 'pending')
                        <center>
                            <p style="font-size:15vw;padding:30px">Belum Mulai</p>
                        </center>
                    @elseif($history->status == 'start')
                        <center>
                            <p id="timer" style="font-size:15vw;padding:30px"></p>
                        </center>
                        <!-- Script Timer -->
                        <script>
                            var x = "<?php echo"$date"?>";
                            var countDownDate = new Date(x).getTime();
                            // Update the count down every 1 second
                            var x = setInterval(function() {
                                // Get todays date and time
                                var now = new Date().getTime();
                                // Find the distance between now an the count down date
                                var distance = countDownDate - now;
                                // Time calculations for days, hours, minutes and seconds
                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                // Output the result in an element with id="demo"
                                document.getElementById("timer").innerHTML = hours + ":"
                                + minutes + ":" + seconds;
                                // If the count down is over, write some text 
                                if (distance < 0) {
                                    clearInterval(x);
                                    document.getElementById("timer").innerHTML = "Time Out";
                                    window.location='/admin/evoting/{{$history->id}}/stop';
                                }
                            }, 1000);
                        </script>
                    @elseif($history->status == 'stop')
                        <center>
                            <p style="font-size:15vw;padding:30px">Selesai</p>
                        </center>
                    @endif
                    <div class="row" style="padding:30px">
                        <div class="col-xl-4 col-md-4 col-sm-12">
                        </div>  
                        <div class="col-xl-4 col-md-4 col-sm-12">
                            @if($history->status == 'start')
                                <a title="Tampilkan Waktu" style="width:100%;"  class="btn btn-danger selesaikan-confirm" href="/admin/evoting/{{$history->id}}/stop">
                                    <span class="btn-inner--icon"><i class="ni ni-button-power" style="margin-right:10px"></i>Selesaikan</span>
                                </a>
                            @elseif($history->status == 'stop')
                                <a title="Statistik Hasil Pemilihan" style="width:100%;"  class="btn btn-danger" href="">
                                    <span class="btn-inner--icon"><i class="ni ni-chart-bar-32" style="margin-right:10px"></i>Hasil Suara</span>
                                </a>
                            @endif
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>
<script>
    var elem = document.getElementById("fullscreen");
    function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
        elem.msRequestFullscreen();
    }
    }
</script>
@endsection

@section('script')
<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.selesaikan-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin mengakhiri voting ?',
            text: 'Voting yang sudah diakhiri tidak bisa dibuka kembali !',
            icon: 'warning',
            buttons: ["Batalkan", "Selesaikan"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection