@extends('layouts.master-guest')

@section('tittle')
Hasil Suara
@endsection

@section('content')
<style>
    .bg-default{
        background-image: url("../assets/landing/images/bg-login-token.png");
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
</style>
<!-- Main content -->
    <?php
        $history_voting = \App\Models\History::all();
        // dd($history_voting);
        $id = 0;
        for($i=0;$i<count($history_voting);$i++){
            if($history_voting[$i]->status == "stop"){
                $id = $history_voting[$i]->id;
            }
        }
        $status = 1 ;
        if ($id != 0){
            $hasil = \App\Models\History::find($id);
            $pilihan = \App\Models\Pilihan::where('id_kegiatan',$hasil->id)->get();
            if(count($pilihan) == 0){
                $status = 0;
            }
            $formatur = \App\Models\Formatur::all();
            $pilihan_all = [];

            for($i=0;$i<count($pilihan);$i++){
                $pilihan_all[$i] = $pilihan[$i]->pilihan;
            }
            sort($pilihan_all);
            $hasil_suara = array_count_values($pilihan_all);

            $formatur_all = \App\Models\Formatur::all();
        }else{
            $status = 0;
        }
    ?>
    @if($status == 0)
        <div style="background:white;margin:30px;padding:20px;padding-top:0px;border-radius:20px;z-index:999999">
            <div class="row">
                <div class="col-12 col-md-3 col-lg-3">
                    <a class="btn btn-sm btn-primary" href="/" style="margin-top:30px">
                        <span class="btn-inner--icon">
                            <i class="ni ni-bold-left" style="margin-right:10px"></i>
                        </span>
                        Kembali
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <h2>
                        <br>
                        <center>
                            <b>Hasil Pemilihan</b>  <br>
                            <small>pada tanggal :</small>
                        </center>
                        <br>
                    </h2>
                </div>
                <div class="col-3 col-md-3 col-lg-3"></div>
            </div>
            <div class="table-responsive">
                <table class="table align-middle">
                    <thead>
                        <tr>
                            <th>
                                No Urut
                            </th>
                            <th>
                                Nama Formatur
                            </th>
                            <th>
                                Asal Jurusan
                            </th>
                            <th>
                                Jumlah Suara
                            </th>
                            <th>
                                <center>
                                Status
                                </center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="5">
                                <center>Data masih diproses</center>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            <p align="center">Pimpinan Daerah Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono </p>
        </div>

    @else
        <div class="row">
        </div>
        <div style="background:white;margin:30px;padding:20px;padding-top:0px;border-radius:20px;z-index:999999">
            <div class="row">
                <div class="col-12 col-md-3 col-lg-3">
                    <a class="btn btn-sm btn-primary" href="/" style="margin-top:30px">
                        <span class="btn-inner--icon">
                            <i class="ni ni-bold-left" style="margin-right:10px"></i>
                        </span>
                        Kembali
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <h2>
                        <br>
                        <center>
                            <b>Hasil Pemilihan {{$hasil->name}}</b>  <br>
                            <small>pada tanggal {{date_format(date_create($hasil->tanggal),"d - M - Y")}}</small>
                        </center>
                        <br>
                    </h2>
                </div>
                <div class="col-3 col-md-3 col-lg-3"></div>
            </div>
            <div class="table-responsive">
                <table class="table align-middle">
                    <thead>
                        <tr>
                            <th>
                                No Urut
                            </th>
                            <th>
                                Nama Formatur
                            </th>
                            <th>
                                Asal Jurusan
                            </th>
                            <th>
                                Jumlah Suara
                            </th>
                            <th>
                                <center>
                                Status
                                </center>
                            </th>
                        </tr>
                    </thead>
                    <?php
                        $no = 1;
                        $hasil_semua = [];
                        foreach ($hasil_suara as $key => $value){
                            array_push($hasil_semua, $key);
                        }
                        $id_formatur_all = [];
                        for($i=0;$i<count($formatur_all);$i++){
                            array_push($id_formatur_all, $formatur_all[$i]->id);
                        }
                        $cari_id_kosong = array_merge($hasil_semua,$id_formatur_all);
                        sort($cari_id_kosong);

                        $nilai_unique = [];
                        $i=0;
                        while($i<count($cari_id_kosong)){
                            if($i == count($cari_id_kosong)-1){
                            array_push($nilai_unique, $cari_id_kosong[$i]);
                            break;
                            }
                            if($cari_id_kosong[$i]!=$cari_id_kosong[$i+1]){
                            array_push($nilai_unique, $cari_id_kosong[$i]);
                            $i = $i+1;
                            }else{
                            $i = $i+2;
                            }
                        }

                        $hasil_semua_key = [];
                        foreach ($hasil_suara as $key => $value){
                            array_push($hasil_semua_key, [$key,$value]);
                        }
                        for($i=0;$i<count($nilai_unique);$i++){
                            array_push($hasil_semua_key, [$nilai_unique[$i],0]);
                        }
                        $nilai_max = [];
                        foreach ($hasil_suara as $key => $value){
                            array_push($nilai_max, $value);
                        }

                        rsort($nilai_max);

                        $id_calon_formatur_terpilih = [];
                        for($i=0;$i<9;$i++){
                            array_push($id_calon_formatur_terpilih, $nilai_max[$i]);
                        }
                    ?>
                    <tbody>
                        <?php
                            $max = \App\Models\History_Access::find(1)->max_data_hasil;
                        ?>
                        @foreach ($formatur_all as $x)
                            @if($x->status == 1)
                                @foreach ($hasil_semua_key as $id)
                                    @if($max > 0)
                                        @if($x->id == $id[0])
                                            <?php
                                                $user = \App\Models\Formatur::find($x->id);
                                            ?>
                                            <tr>
                                                <td width="10px">
                                                    <center>
                                                        {{$user->no_formatur}}
                                                    </center>
                                                </td>
                                                <td>
                                                    <a href="{{$x->getFoto()}}" target="_blank" tittle="{{$x->foto}}" >
                                                        <img src="{{$x->getFoto()}}" width="50px" style="border-radius:100px;margin-right:10px" height="50px" alt="$x->foto">
                                                    </a>
                                                    {{$user->name}}
                                                </td>
                                                <td>{{$user->id_daerah}}</td>
                                                <td>
                                                    {{$id[1]}} Suara
                                                </td>
                                                <td>
                                                <center>
                                                    @if($id[1] == $id_calon_formatur_terpilih[0] or $id[1] == $id_calon_formatur_terpilih[1] or $id[1] == $id_calon_formatur_terpilih[2])
                                                        <p class="btn btn-sm btn-success">Lolos</p>
                                                    @elseif($id[1] == $id_calon_formatur_terpilih[3] or $id[1] == $id_calon_formatur_terpilih[4] or $id[1] == $id_calon_formatur_terpilih[5])
                                                        <p class="btn btn-sm btn-success">Lolos</p>
                                                    @elseif($id[1] == $id_calon_formatur_terpilih[6] or $id[1] == $id_calon_formatur_terpilih[7] or $id[1] == $id_calon_formatur_terpilih[8])
                                                        <p class="btn btn-sm btn-success">Lolos</p>
                                                    @else
                                                        <p class="btn btn-sm btn-dark">Tidak</p>
                                                    @endif
                                                </center>
                                                </td>
                                            </tr>
                                            <?php
                                                $max--;
                                            ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <p align="center">Pimpinan Daerah Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono </p>
        </div>
    @endif
@endsection
