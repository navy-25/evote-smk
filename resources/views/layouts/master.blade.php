<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Aplikasi e-Voting PR IPM SMKM 1 Kts. Dibuat untuk keperluan khusus musyawarah tingkat daerah ikatan pelajar muhammadiyah SMKM 1 Kertosono.">
        <meta name="author" content="IPR IPM SMKM 1 Kts">
        <title>e-Vote IPM SMKM 1 Kts | @yield('tittle')</title>
        <!-- Favicon -->
        <link rel="icon" href="{{asset('/assets/img/brand/favicon.png')}}" type="image/png">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <!-- Icons -->
        <link rel="stylesheet" href="{{asset('/assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
        <!-- Argon CSS -->
        <link rel="stylesheet" href="{{asset('/assets/css/argon.css?v=1.2.0')}}" type="text/css">

        <!-- j querry -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'"></script>
    </head>
    <body>
        <style>
            /* width */
            ::-webkit-scrollbar {
            width: 10px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px dark;
            border-radius: 10px;
            }

            /* Handle */
            ::-webkit-scrollbar-thumb {
            background: #32325d;
            border-radius: 10px;
            }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
            background: #1e1e44;
            }
        </style>
        <!-- Sidenav -->
        <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
            <div class="scrollbar-inner">
            <!-- Brand -->
            <div class="sidenav-header  align-items-center" >
                <a class="navbar-brand text-left" href="{{ route('home') }}">
                    e-Voting
                    <br> <b style="color:#5e72e4 !important">IPM SMKM 1 Kts</b>
                    {{-- <img src="{{asset('/assets/img/brand/blue.png')}}" class="navbar-brand-img" alt="..."> --}}
                </a>
                <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
            <div class="navbar-inner">
                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-home')" href="{{ route('home') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Beranda</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-calon_formatur')" href="{{ route('calon_formatur') }}">
                            <i class="ni ni-single-02 text-primary"></i>
                            <span class="nav-link-text">Calon Formatur</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-token')" href="{{ route('token') }}">
                            <i class="ni ni-atom text-primary"></i>
                            <span class="nav-link-text">Token Pilih</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-evoting')" href="{{ route('evoting') }}">
                            <i class="ni ni-app text-primary"></i>
                            <span class="nav-link-text">Evoting</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-feedback')" href="{{ route('feedback') }}">
                            <i class="ni ni-chat-round text-primary"></i>
                            <span class="nav-link-text">Feedback</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-pengaturan')" href="{{ route('pengaturan') }}">
                            <i class="ni ni-building text-primary"></i>
                            <span class="nav-link-text">Kelola Jurusan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @yield('nav-evote')" href="/">
                            <i class="ni ni-world-2 text-primary"></i>
                            <span class="nav-link-text">Visit E-Voting</span>
                        </a>
                    </li>
                </ul>
                <!-- Divider -->
                <hr class="my-3">
                <!-- Heading -->
                    @if(Auth::user()->status == 'root')
                    <!--@if(Auth::user()->name == 'Unknown')-->
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link @yield('nav-role_list')" href="{{route('role_list')}}">
                                <i class="ni ni-glasses-2 text-primary"></i>
                                <span class="nav-link-text">Data Akun</span>
                            </a>
                        </li>
                    </ul>
                    <!--@endif-->
                    {{-- <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link @yield('nav-realtime')" href="{{route('realtime')}}">
                                <i class="ni ni-chart-bar-32 text-primary"></i>
                                <span class="nav-link-text">Realtime Result</span>
                            </a>
                        </li>
                    </ul> --}}
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link @yield('nav-duplicate_check')" href="{{route('duplicate_check')}}">
                                <i class="ni ni-single-02 text-primary"></i>
                                <span class="nav-link-text">Duplicate Check</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link @yield('nav-root')" href="{{route('data_pemilihan_root')}}">
                                <i class="ni ni-folder-17 text-primary"></i>
                                <span class="nav-link-text">Restore data</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
            </div>
        </nav>
        <!-- Main content -->
        <div class="main-content" id="panel">
            <!-- Topnav -->
            <nav class="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Search form -->
                    @yield('search')

                    <!-- Navbar links -->
                    <ul class="navbar-nav align-items-center  ml-md-auto ">
                        <li class="nav-item d-xl-none">
                        <!-- Sidenav toggler -->
                        <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                            <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            </div>
                        </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                        <li class="nav-item dropdown">
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle" style="background:none">
                                        <img alt="Image placeholder" style="border:2px solid #32325d" src="/assets/img/brand/brand.jpg">
                                    </span>
                                    <div class="media-body  ml-2  d-none d-lg-block">
                                        <span class="mb-0 text-sm  font-weight-bold">{{Auth::user()->name}}</span>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu  dropdown-menu-right ">
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Assalamu'alaikum !</h6>
                                </div>
                                <a href="{{ route('profile') }}" class="dropdown-item">
                                    <i class="ni ni-single-02"></i>
                                    <span>Profil saya</span>
                                </a>
                                <!-- <div class="dropdown-divider"></div> -->
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="ni ni-user-run"></i>
                                    <span>Logout</span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
            @yield('content')
        </div>
        <!-- Argon Scripts -->
        <!-- Core -->

        <script src="{{asset('/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('/assets/vendor/js-cookie/js.cookie.js')}}"></script>
        <script src="{{asset('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
        <script src="{{asset('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
        <!-- Argon JS -->
        <script src="{{asset('/assets/js/argon.js?v=1.2.0')}}"></script>

        <!-- alert -->
        <script src="{{asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js')}}"></script>

        <!-- modal -->
        <script type="text/javascript">
            $(document).ready(function () {
                /* Show customer */
                $('body').on('click', '#show-customer', function () {
                    $('#customerCrudModal-show').html("Customer Details");
                    $('#crud-modal-show').modal('show');
                });
            });
            function exportTableToExcel(tableID, filename = ''){
                var downloadLink;
                var dataType = 'application/vnd.ms-excel';
                var tableSelect = document.getElementById(tableID);
                var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

                // Specify file name
                filename = filename?filename+'.xls':'excel_data.xls';

                // Create download link element
                downloadLink = document.createElement("a");

                document.body.appendChild(downloadLink);

                if(navigator.msSaveOrOpenBlob){
                    var blob = new Blob(['\ufeff', tableHTML], {
                        type: dataType
                    });
                    navigator.msSaveOrOpenBlob( blob, filename);
                }else{
                    // Create a link to the file
                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
                }
            }
        </script>
        @yield('script')
    </body>
</html>
