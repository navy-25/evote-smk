<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History_Access extends Model
{
    use HasFactory;
    protected $table = 'history_access';
    protected $primaryKey = 'id';
    protected $fillable = [
        'status_akses',
        'max_data_hasil'
    ];
}
