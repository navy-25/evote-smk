@extends('layouts.master')

@section('nav-root')
    active
@endsection

@section('tittle')
    Root Backup Data
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>    
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">    
        <div class="col-8">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-4">
                            <h3 class="mb-0">
                                Hasil suara [{{count($data_pilihan)}}]
                            </h3>
                        </div>
                        <div class="col-8 text-right">
                            <form action="{{route('data_pemilihan_root_filter')}}">
                                <select style="border-radius:10px;font-size:12px;height:30px;margin-right:6px" name="formatur">
                                    <option value="">Semua Formatur</option>
                                    @foreach($data_formatur as $f)
                                        <option value="{{$f->id}}">{{$f->name}}</option>
                                    @endforeach
                                </select>
                                <select style="border-radius:10px;font-size:12px;height:30px;margin-right:6px" name="kegiatan">
                                    <option value="">Pilih Kegiatan</option>
                                    @foreach($data_kegiatan as $d)
                                        <option value="{{$d->id}}">{{$d->name}}</option>
                                    @endforeach
                                </select>
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">filter</button>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:25%">Token</th>
                                <th scope="col" style="width:45%">Kegiatan</th>
                                <th scope="col" style="width:25%">Pilihan</th>
                                <th scope="col" style="width:25%">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($data_pilihan as $x)
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>{{$x->token}}</td>
                                <td>{{$x->nama_kegiatan}} ({{$x->id_kegiatan}})</td>
                                <td>{{$x->nama_formatur}} ({{$x->pilihan}})</td>
                                <td>{{$x->created_at}}</td>
                            </tr>
                            @endforeach
                            @if(count($data_pilihan) == 0)
                                <tr>
                                    <th colspan="5">
                                        <center>Tidak ada data</center> 
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">
                                Restore data
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <?php
                        // read
                        $pilihanCSV = file('backups/pilih.csv');
                        $data = [];
                        foreach ($pilihanCSV as $line) {
                            $data [] = str_getcsv($line);
                        }
                        // end read 
                        if(count($data) != 0){
                            $tanggal = $data[count($data)-1][5];
                            $status = "ready";
                        }else{
                            $tanggal = 'Kosong';
                            $status = "not ready";
                        }
                    ?>
                    <table class="table align-items-center table-flush">
                        <thead>
                            <tr>
                                <th >Jumlah Data</th>
                                <th >:</th>
                                <th >{{count($data)}}</th>
                            </tr>
                            <tr>
                                <th >Tanggal</th>
                                <th >:</th>
                                <th >{{$tanggal}}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="card-footer border-0">
                    @if($status == "ready")
                        <a class="btn btn-danger restore-confirm" href="{{route('restore_data')}}" style="color:white;width:100%;margin-bottom:10px"><i class="ni ni-cloud-upload-96 mr-1"></i>Restore data</a>
                        <a class="btn btn-success" href="{{asset('/backups/pilih.csv')}}" style="color:white;width:100%"><i class="ni ni-cloud-download-95 mr-1"></i>Unduh Backups</a>
                    @else
                        <button class="btn btn-warning" style="color:white;width:100%">Can't Restore</button>
                    @endif
                </div>
            </div>
        </div>   
    </div>
    
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection

@section('script')
<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.restore-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Restore data ?',
            text: 'Data yang direstore akan ditimbun dan diganti dengan data terakhir backups',
            icon: 'warning',
            buttons: ["Batalkan", "Restore"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection