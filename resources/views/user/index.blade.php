@extends('layouts.master-guest')

@section('tittle')
Pilih Formatur
@endsection

@section('content')
<!-- Footer -->
<style>
    .row {
        display: flex;
        margin-right: 0px;
        margin-left: 0px;
        flex-wrap: wrap;
    }
    .bg-default{
        background-image: url("{{asset('/assets/landing/images/bg-login-token.png')}}");
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
</style>
<form method="POST" action="/login-token/vote-now/{{$token_pakai->id}}/store">
    @csrf
    <div class="row justify-content-center" style="margin-top:10px;background-color:none;width:100%;height:100px" >
        <div class="col-xl-4"></div>
        <div class="col-xl-4 text-center pt-5" style="font-size:20px;color: white">
            {{-- <img src="{{asset('/assets/img/brand/white.png')}}" width="300px" style="display: block;margin-top:30px;margin-left: auto;margin-right: auto;"> --}}
            e-Voting <br>
            <b>IPM SMKM 1 Kts</b>
        </div>
        <div class="col-xl-4"></div>
    </div>
    <div class="container mb-5">
        <div class="alert alert-secondary alert-dismissible text-center" style="margin-top:40px;" role="alert">
            <span class="alert-text">Jangan lupa untuk melakukan pengecekan ulang terhadap kandidat yang dipilih !</span>
        </div>
        @if ($message = Session::get('gagal'))
            <div class="alert alert-danger alert-dismissible" style="margin-top:40px;" role="alert">
                <span class="alert-text">{{$message}}</span>
            </div>
        @endif
        <div class="row justify-content-center" style="padding:10px;margin-top:40px;">
            <?php
                $no = 1;
            ?>
            @foreach($formatur as $x)
                @if($x->status == 1)
                    <?php
                        $nama = $x->name;
                        $nama = explode(" ",$nama);

                    ?>
                    <div class="form-check">
                        <label class="form-check-label" for="flexCheckDefault{{$no++}}">
                            <div class="card" style="width:170px;min-height:300px !important">
                                <img class="card-img-top" src="{{$x->getFoto()}}" height="170px" alt="{{$x->name}}">
                                <div class="card-body">
                                    <center>
                                        <small>Formatur {{$x->no_formatur}}</small>
                                        <div>
                                            <h5 class="card-title" style="margin-bottom:5px">
                                                {{$x->id_daerah}}
                                            </h5>
                                        </div>
                                    </center>
                                    <div style="height:40px">
                                        <small class="btn btn-sm btn-warning" style="font-size:10px;width:100%">{{$x->name}}</small>
                                    </div>
                                    <input class="" style="width:100%;margin-top:30px;height:50px" name="pilih[]" type="checkbox" value="{{$x->id}}" id="flexCheckDefault{{$no++}}">
                                </div>
                            </div>
                        </label>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="row justify-content-center" style="position:fixed;bottom:10px;background-color:none;width:100%;height:50px" >
        <div class="col-xl-4"></div>
        <div class="col-xl-4">
            <center>
                <button class="btn btn-sm btn-warning"
                    style="width:70%;font-size:20px" type="submit">
                    <span class="btn-inner--icon"><i class="ni ni-send"></i></span>
                    Kirim Pilihan
                </button>
            </center>
        </div>
        <div class="col-xl-4"></div>
    </div>
</form>
@endsection
