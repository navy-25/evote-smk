@extends('layouts.master')

@section('nav-pengaturan')
    active
@endsection

@section('tittle')
    Edit Pimpinan
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<!-- <div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-image: url(/assets/img/theme/profile-cover2.jpg); background-size: cover; background-position: center top;"> -->
    <!-- <span class="mask bg-gradient-default opacity-8"></span> -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-12 col-12">
                    @if ($message = Session::get('gagal'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @elseif ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @endif
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('pengaturan') }}">Pengaturan</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Pimpinan</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-6 col-md-6 col-sm-6">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="/admin/pengaturan/daerah_update/{{$pimpinan->id}}">
                        @csrf
                        <div class="pl-lg">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Nama Jurusan</label>
                                        <input type="text" name="name" class="form-control" value="{{$pimpinan->name}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Jumlah Peserta</label>
                                        <input type="number" name="jumlah" class="form-control" value="{{$pimpinan->jumlah}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <a href="/admin/pengaturan/" class="btn btn-sm btn-secondary">Tutup</a>
                                </div>
                                <div class="col-6 text-right">
                                    <span>
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Konfirmasi
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection
