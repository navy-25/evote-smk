<!DOCTYPE html>
<html>
<head>
	<title >e-Vote IPM SMKM 1 Kts | Cetak Data Token</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/jpg" href="{{asset('/assets/img/brand/favicon.png')}}"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
    <body>
        <style type="text/css">
            html{
                font-family:Arial;
            }
            table tr td,
            table tr th,body,pre{
                font-size: 9pt;

            }
            body{
                padding:5px;
            }
        </style>
        <center>
            <h5>Data Token Pemilihan <BR>Ikatan Pelajar Muhammadiyah SMKM 1 Kertosono</h5>
        </center>
        <br>
        <table class='table table-bordered'>
            <thead>
                <tr>
                    <th style="width:5%">
                        <center>
                        No
                        </center>
                    </th>
                    <th style="width:50%">
                        Token
                    </th>
                    <th style="width:45%">
                        Status
                    </th>
                </tr>
            </thead>
            <?php
                $no = 1;
            ?>
            <tbody>
                @foreach($token as $p)
                    @if($p->akses != "admin")
                    <tr>
                        <td style="width:5%">
                            <center>
                                {{$no++}}
                            </center>
                        </td>
                        <td style="width:50%">
                            {{$p->name}}
                        </td>
                        @if($p->status == "0")
                            <td style="width:50%">Sudah Dipakai</td>
                        @elseif($p->status == 1)
                            <td style="width:45%">Siap Dipakai</td>
                        @endif
                    </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </body>
</html>
