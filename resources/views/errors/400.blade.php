<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Aplikasi e-Voting PR IPM SMKM 1 Kts. Dibuat untuk keperluan khusus musyawarah tingkat daerah ikatan pelajar muhammadiyah SMKM 1 Kertosono.">
    <meta name="author" content="IPR IPM SMKM 1 Kts">
	<!-- Favicon -->
	<link rel="icon" href="/assets/img/brand/favicon.png" type="image/png">
	<title>400 Error</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:700,900" rel="stylesheet">

	<!-- Font Awesome Icon -->
	<link type="text/css" rel="stylesheet" href="/assets/error/css/font-awesome.min.css" />

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="/assets/error/css/style.css" />
</head>
<body>
    <style>
    #notfound .notfound-bg:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(58, 13, 179, 0.7);
    }
    </style>
	<div id="notfound">
		<div class="notfound-bg"></div>
		<div class="notfound">
			<div class="notfound-404">
				<h1>400</h1>
			</div>
			<h2>Wah kayaknya bukan kamu deh adminya, balik lagi aja ya !</h2>
			<a href="/" class="home-btn" style="color:#5e72e4">Kembali</a>
			{{-- <a href="https://ipmjatim.or.id/" class="contact-btn">Official Website</a>
			<div class="notfound-social">
				<a target="_blank" href="https://www.instagram.com/ipm_jatim/"><i class="fa fa-instagram"></i></a>
				<a target="_blank" href="https://www.facebook.com/ipmjawatimur/"><i class="fa fa-facebook"></i></a>
				<a target="_blank" href="https://twitter.com/ipmjatim1"><i class="fa fa-twitter"></i></a>
				<a target="_blank" href="https://www.youtube.com/channel/UCx-f0CXSRFm27WvnHdFhQAA"><i class="fa fa-youtube-play"></i></a>
			</div> --}}
		</div>
	</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
