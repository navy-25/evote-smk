@extends('layouts.master')

@section('nav-evoting')
    active
@endsection

@section('tittle')
    Monitoring Pemilihan
@endsection

@section('search')

@endsection

@section('content')
<!-- <div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-image: url(/assets/img/theme/profile-cover2.jpg); background-size: cover; background-position: center top;"> -->
    <!-- <span class="mask bg-gradient-default opacity-8"></span> -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>    
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Monitoring Pemilihan</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <button class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-history">+ Buat Voting</button> -->
                    <!-- <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <!-- <h3 class="mb-0">Monitoring Pemilihan</h3> -->
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-history">+ Buat Voting</button>
                            <button class="btn btn-sm btn-warning">
                                <?php
                                    $akses_hasil = \App\Models\History_Access::find(1);
                                ?>
                                <i class="ni ni-single-02 mr-1"></i> : {{$akses_hasil->max_data_hasil}}
                            </button>
                        </div>
                        <div class="col text-right">
                            <a href="/admin/evoting/aktifkan_hasil" class="btn btn-sm btn-secondary confirm-history-aktivation">
                                <?php
                                    $akses_hasil = \App\Models\History_Access::find(1);
                                ?>
                                @if($akses_hasil->status_akses == 1)
                                    Pending Hasil suara
                                @else
                                    Tampilkan Hasil suara
                                @endif
                            </a>
                            <a href="/admin/evoting/batasi_formatur" data-toggle="modal" data-target="#modal-batas" class="btn btn-sm btn-warning confirm-history-aktivation">
                                <?php
                                    $akses_hasil = \App\Models\History_Access::find(1);
                                ?>
                                <i class="ni ni-single-02 mr-1"></i> Set batas
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:10%">Tanggal</th>
                                <th scope="col" style="width:20%">Nama Kegiatan</th>
                                <th scope="col" style="width:10%">Status</th>
                                <th scope="col" style="width:10%">Waktu Mulai</th>
                                <th scope="col" style="width:10%">Waktu Selesai</th>
                                <th scope="col" style="width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($history as $x)
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>
                                    <?php
                                        $date = date_create($x->tanggal);
                                        $dates = date_format($date,'F d, Y');
                                        $date_today = date('F d, Y');
                                    ?>
                                    {{$dates}}
                                </td>
                                <td>{{$x->name}}</td>
                                <td>
                                @if($dates != $date_today)
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-danger"></i>
                                        <span class="status">Expired</span>
                                    </span>
                                @else
                                    @if($x->status == 'pending')
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-warning"></i>
                                            <span class="status">Pending</span>
                                        </span>
                                    @elseif($x->status == 'start')
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-success"></i>
                                            <span class="status">Start</span>
                                        </span>
                                    @elseif($x->status == 'stop')
                                        <span class="badge badge-dot mr-4">
                                            <i class="bg-danger"></i>
                                            <span class="status">Selesai</span>
                                        </span>
                                    @endif
                                @endif
                                </td>
                                <td>{{$x->waktu_mulai}}</td>
                                <td>{{$x->waktu_akhir}}</td>
                                <td>
                                    @if($dates != $date_today)
                                        @if($x->status == 'stop')
                                            <a title="Statistik Hasil Pemilihan"  class="btn btn-sm btn-icon btn-warning" href="/admin/evoting/{{$x->id}}/hasil">
                                                <span class="btn-inner--icon"><i class="ni ni-chart-bar-32"></i></span>
                                            </a>
                                            <a title="Hapus pemilihan" class="btn btn-sm btn-icon btn-danger detele-confirm" href="/admin/evoting/{{$x->id}}/hapus">
                                                <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                            </a>
                                        @else
                                            <a  class="btn btn-sm btn-icon btn-danger" style="color:white">
                                                <span class="btn-inner--icon">x</span>
                                            </a>
                                        @endif
                                    @elseif($x->status == 'pending')
                                        @if($dates == $date_today)
                                            <a title="Mulai"  class="btn btn-sm btn-icon btn-success mulai-confirm" href="/admin/evoting/{{$x->id}}/start">
                                                <span class="btn-inner--icon"><i class="ni ni-button-play"></i></span>
                                            </a>
                                        @endif
                                        <a title="Hapus pemilihan" class="btn btn-sm btn-icon btn-danger detele-confirm" href="/admin/evoting/{{$x->id}}/hapus">
                                            <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                        </a>
                                    @elseif($x->status == 'start')
                                        <a title="Stop Voting"  class="btn btn-sm btn-icon btn-danger" href="/admin/evoting/{{$x->id}}/stop">
                                            <span class="btn-inner--icon"><i class="ni ni-button-power"></i></span>
                                        </a>
                                        <a title="Tampilkan Waktu"  class="btn btn-sm btn-icon btn-primary" href="/admin/evoting/{{$x->id}}/timer">
                                            <span class="btn-inner--icon"><i class="ni ni-time-alarm"></i></span>
                                        </a>
                                    @elseif($x->status == 'stop')
                                        <a title="Statistik Hasil Pemilihan"  class="btn btn-sm btn-icon btn-warning" href="/admin/evoting/{{$x->id}}/hasil">
                                            <span class="btn-inner--icon"><i class="ni ni-chart-bar-32"></i></span>
                                        </a>
                                        <a title="Hapus pemilihan" class="btn btn-sm btn-icon btn-danger detele-confirm" href="/admin/evoting/{{$x->id}}/hapus">
                                            <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @if(count($history) == 0)
                                <tr>
                                    <th colspan="7">
                                        <center>Tidak ada data</center> 
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="modal-history" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2><i class="ni ni-settings-gear-65" style="margin-right:10px"></i> Setting pemilihan</h2>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('history-store')}}">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Nama Kegiatan</label>
                                    <input type="text" name="name" class="form-control"placeholder="Masukkan Nama Kegiatan">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Tanggal Pelaksanaan</label>
                                    <input type="date" name="tanggal" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Waktu Mulai</label>
                                    <input type="time" name="waktu_mulai" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Waktu Akhir</label>
                                    <input type="time" name="waktu_akhir" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-batas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" action="/admin/evoting/batasi_formatur">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="alert alert-success" role="alert">
                                    <span class="alert-text">Jumlah ini digunakan untuk membatasi jumlah formatur yang tampil pada hasil suara terpilih</span>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Jumlah formatur yang tampil</label>
                                    <input type="number" name="batas_formatur" class="form-control"placeholder="Masukkan pengaturan batas">
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin dihapus ?',
            text: 'Kegiatan yang dihapus tidak bisa direcovery kembali',
            icon: 'warning',
            buttons: ["Batalkan", "Hapus"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>

<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.mulai-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin mulai voting ?',
            text: 'Jika sudah dimulai maka waktu akan terhitung dari titik mulai',
            icon: 'warning',
            buttons: ["Batalkan", "Mulai"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection