<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
        $cari = $request->cari;
        if($cari != null){
            $feedback = \App\Models\Feedback::where('name','like',"%".$cari."%")
            ->orWhere('email','like',"%".$cari."%")
            ->orWhere('pesan','like',"%".$cari."%")
            ->orderby('id','DESC')->paginate(100);
        }else{
            $feedback = \App\Models\Feedback::orderby('id','DESC')->paginate(100);
        }
        return view('feedback.index',compact('feedback'));
    }
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $feedback = \App\Models\Feedback::create([        
                'name' => $request->name,
                'email' => $request->email,
                'pesan' => $request->pesan,
            ]);
            return redirect('/')->with(['success' => 'berhasil']);
        }catch (Exception $e){
            return redirect('/')->with(['gagal' => 'gagal']);
        }
    }
    public function show($id)
	{
        $feedback = \App\Models\Feedback::find($id);
        return Response::json($feedback);
	}
}
