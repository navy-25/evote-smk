<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/login-admin', function () {
//     return view('auth.login-admin');
// });

Route::get('/error', function () {
    return view('errors.400');
})->name('errors');

Route::get('/hasil-suara', function () {
    $akses_hasil = \App\Models\History_Access::find(1);
    if ($akses_hasil->status_akses == 1) {
        return view('user.hasil_suara');
    } else {
        return redirect('/');
    }
});

// Auth::routes();
// Authentication Routes...
Route::get('/login-admin', [App\Http\Controllers\Auth\LoginController::class, 'login_admin'])->name('login');
Route::post('/login-admin', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::post('/kirim-feedback', [App\Http\Controllers\FeedbackController::class, 'store'])->name('kirim-pesan');

// Registration Routes...
// Route::get('/register-admin', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
// Route::post('/register-admin', [App\Http\Controllers\Auth\RegisterController::class, 'register']);

// Password Reset Routes...
// Route::get('password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class,'showLinkRequestForm'])->name('password.request');
// Route::post('password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
// Route::get('password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class,'showResetForm'])->name('password.reset');
// Route::post('password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class,'reset']);

Route::get('/login-token', [App\Http\Controllers\GuestController::class, 'login']);

Route::post('/login-token/vote-now/', [App\Http\Controllers\GuestController::class, 'login_verifikasi'])->name('login-token');
Route::post('/login-token/vote-now/{id}/store', [App\Http\Controllers\GuestController::class, 'vote']);
Route::get('/login-token/vote-now/{name}', [App\Http\Controllers\GuestController::class, 'vote_now']);

Route::group(['middleware' => ['auth', 'checkStatus:admin,root']], function () {
    Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::post('/admin/profile/{id}/update', [App\Http\Controllers\HomeController::class, 'update']);
    Route::get('/admin/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');
    Route::post('/admin/profile/ganti-password', [App\Http\Controllers\HomeController::class, 'ganti_password'])->name('ganti-password');

    Route::get('/admin/calon-formatur', [App\Http\Controllers\Calon_FormaturController::class, 'index'])->name('calon_formatur');
    Route::post('/admin/calon-formatur/formatur-store', [App\Http\Controllers\Calon_FormaturController::class, 'store'])->name('formatur-store');
    Route::get('/admin/calon-formatur/{id}/ubah-status', [App\Http\Controllers\Calon_FormaturController::class, 'deactive']);
    Route::get('/admin/calon-formatur/{id}', [App\Http\Controllers\Calon_FormaturController::class, 'edit']);
    Route::get('/admin/calon-formatur/{id}/hapus-foto', [App\Http\Controllers\Calon_FormaturController::class, 'hapus_foto']);
    Route::post('/admin/calon-formatur/{id}/update', [App\Http\Controllers\Calon_FormaturController::class, 'update']);
    Route::get('/admin/calon-formatur/hapus-calon/{id}', [App\Http\Controllers\Calon_FormaturController::class, 'hapus']);

    Route::get('/admin/pengaturan', [App\Http\Controllers\PengaturanController::class, 'index'])->name('pengaturan');
    Route::post('/admin/pengaturan/daerah_store', [App\Http\Controllers\PengaturanController::class, 'daerah_store'])->name('daerah_store');
    Route::get('/admin/pengaturan/daerah_edit/{id}', [App\Http\Controllers\PengaturanController::class, 'daerah_edit']);
    Route::get('/admin/pengaturan/daerah_hapus/{id}', [App\Http\Controllers\PengaturanController::class, 'destroy']);
    Route::post('/admin/pengaturan/daerah_update/{id}', [App\Http\Controllers\PengaturanController::class, 'daerah_update']);

    Route::get('/admin/token', [App\Http\Controllers\TokenController::class, 'index'])->name('token');
    Route::post('/admin/token/token-store', [App\Http\Controllers\TokenController::class, 'store'])->name('token-store');
    Route::get('/admin/token/token-delete', [App\Http\Controllers\TokenController::class, 'destroy'])->name('token-delete');
    Route::get('/admin/token/status', [App\Http\Controllers\TokenController::class, 'status'])->name('token-status');
    Route::get('/admin/token/cetak', [App\Http\Controllers\TokenController::class, 'print'])->name('token-cetak');

    Route::get('/admin/evoting', [App\Http\Controllers\EvotingController::class, 'index'])->name('evoting');
    Route::get('/admin/evoting/aktifkan_hasil', [App\Http\Controllers\EvotingController::class, 'aktifkan_hasil']);
    Route::post('/admin/evoting/batasi_formatur', [App\Http\Controllers\EvotingController::class, 'batasi_formatur']);

    Route::get('/admin/evoting/{id}/timer', [App\Http\Controllers\EvotingController::class, 'timer']);
    Route::get('/admin/evoting/{id}/start', [App\Http\Controllers\EvotingController::class, 'start']);
    Route::get('/admin/evoting/{id}/hapus', [App\Http\Controllers\EvotingController::class, 'destroy']);
    Route::get('/admin/evoting/{id}/stop', [App\Http\Controllers\EvotingController::class, 'stop']);
    Route::get('/admin/evoting/{id}/hasil', [App\Http\Controllers\EvotingController::class, 'barchart']);
    Route::get('/admin/evoting/{id}/hasil/print', [App\Http\Controllers\EvotingController::class, 'print']);
    Route::post('/admin/evoting/history-store', [App\Http\Controllers\EvotingController::class, 'store'])->name('history-store');

    Route::get('/admin/feedback', [App\Http\Controllers\FeedbackController::class, 'index'])->name('feedback');
    Route::get('/admin/feedback/{id}/', [App\Http\Controllers\FeedbackController::class, 'show']);
});

Route::group(['middleware' => ['auth', 'checkStatus:root']], function () {
    Route::get('/admin/root/', [App\Http\Controllers\RootController::class, 'index'])->name('data_pemilihan_root');
    Route::get('/admin/root/duplicate_check', [App\Http\Controllers\RootController::class, 'duplicate_check'])->name('duplicate_check');
    Route::get('/admin/root/duplicate_check/{id}/delete', [App\Http\Controllers\RootController::class, 'delete_duplicate_check'])->name('delete_duplicate_check');
    Route::get('/admin/root/role', [App\Http\Controllers\RootController::class, 'role_list'])->name('role_list');
    Route::get('/admin/root/role/{id}/destroy', [App\Http\Controllers\RootController::class, 'hapus_role']);
    Route::get('/admin/root/filter', [App\Http\Controllers\RootController::class, 'filter'])->name('data_pemilihan_root_filter');
    Route::get('/admin/root/restore_data', [App\Http\Controllers\RootController::class, 'restore_data'])->name('restore_data');
    Route::get('/admin/root/realtime', [App\Http\Controllers\RootController::class, 'realtime'])->name('realtime');
});
