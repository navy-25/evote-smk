<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function login()
    {
        return view('user.login-token');
    }
    public function finish()
    {
        return view('user.finish');
    }
    public function login_verifikasi(Request $request)
    {   
        date_default_timezone_set('Asia/Jakarta');
        $all_history = \App\Models\History::all();
        $check_aktif_pemilihan = [];
        if(count($all_history)==null){
            return redirect('/login-token')->with(['gagal' => 'Kegiatan evoting belum dibuat/mulai']);
        }else{
            for($i=0;$i<count($all_history);$i++){
                if($all_history[$i]->status == 'start'){
                    array_push($check_aktif_pemilihan,1);
                }else{
                    array_push($check_aktif_pemilihan,0);
                }
            }
        }
        if(array_sum($check_aktif_pemilihan) == 0){
            return redirect('/login-token')->with(['gagal' => 'Kegiatan evoting belum dibuat/mulai']);
        }
        $token = \App\Models\Token::all();
        $token_user = $request->token;
        try{
            for($i=0;$i<count($token);$i++){
                if($token_user == $token[$i]->name){
                    $id_token = $token[$i]->id;
                    break;
                }else{
                    $id_token = "gagal";
                }
            }
            if($id_token != "gagal"){
                $token_pakai = \App\Models\Token::find($id_token);
                if($token_pakai->status == 1){
                    $formatur = \App\Models\Formatur::orderby('no_formatur','asc')->get();
                    return redirect('/login-token/vote-now/'.$token_pakai->name);
                }else{
                    return redirect('/login-token')->with(['gagal' => 'Token sudah digunakan, hubungi admin/panlih']);    
                }
            }else{
                return redirect('/login-token')->with(['gagal' => 'Token salah, hubungi admin/panlih']);
            }
        } catch (\Throwable $th) {
            return redirect('/login-token')->with(['gagal' => 'Token salah, hubungi admin/panlih']);
        }
    }
    public function vote_now(Request $request,$id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $formatur = \App\Models\Formatur::orderby('no_formatur','asc')->get();
        $token = \App\Models\Token::all();
        for($i=0;$i<count($token);$i++){
            if($id == $token[$i]->name){
                $id_token = $token[$i]->id;
                break;
            }else{
                $id_token = "gagal";
            }
        }
        $token_pakai = \App\Models\Token::find($id_token);
        return view('user.index',compact('formatur','token_pakai'));
    }
    public function vote(Request $request,$id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $token_pakai = \App\Models\Token::find($id);
        $name = $token_pakai->name;
        $all_history = \App\Models\History::all();
        for($i=0 ;$i<count($all_history);$i++){
            if($all_history[$i]->status == 'start'){
                $id_history = $all_history[$i]->id;
                break;
            }else{
                $id_history = "kosong";
            }
        }
        $pilihan = $request->pilih;
        if($pilihan != null){
            $pilihan = $pilihan;
        }else{
            return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Anda belum memilih']);
        }

        if($id_history != 'kosong'){
            $history = \App\Models\History::find($id_history);
            $history_today = $history;
            $date_today = date('F d, Y');
            $time_today = date('H:i');
            $data_pemilihan = date_format(date_create($history_today->tanggal),'F d, Y');
            
            $waktu_today = explode(':',$time_today);
            $waktu_mulai = explode(':',$history_today->waktu_mulai);
            $waktu_akhir = explode(':',$history_today->waktu_akhir);
            $waktu_today_to_menit = (int)$waktu_today[0]*60 + $waktu_today[1];
            $waktu_mulai_to_menit = (int)$waktu_mulai[0]*60 + $waktu_mulai[1];
            $waktu_akhir_to_menit = (int)$waktu_akhir[0]*60 + $waktu_akhir[1];
            
            // dd($date_today,$data_pemilihan,$waktu_today_to_menit,$waktu_mulai_to_menit);
            if($date_today == $data_pemilihan and $waktu_today_to_menit < $waktu_mulai_to_menit){
                return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Pemilihan belum dimulai']);
            }elseif($date_today == $data_pemilihan and $waktu_today_to_menit >= $waktu_mulai_to_menit and $waktu_today_to_menit < $waktu_akhir_to_menit){
                if(count($pilihan) < 9){
                    $jumlah = count($pilihan);
                    $kurang = 9 - $jumlah;
                    return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Pilihan anda kurang '.$kurang.'. Silahkan ulangi lagi !']);
                }elseif(count($pilihan) > 9){
                    $jumlah = count($pilihan);
                    $kurang = $jumlah - 9;
                    return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Pilihan anda kelebihan '.$kurang.'. Wajib memilih 9 formatur !']);
                }elseif(count($pilihan) == 9){
                    // $pilihan = $pilihan[0].",".$pilihan[1].",".$pilihan[2].",".$pilihan[3].",".$pilihan[4].",".$pilihan[5].",".$pilihan[6].",".$pilihan[7].",".$pilihan[8];
                    try{
                        for($i=0;$i<count($pilihan);$i++){
                            if($history->status == 'start'){
                                $pilihan_store = \App\Models\Pilihan::create([        
                                    'token' => $token_pakai->name,
                                    'id_kegiatan' => $id_history,
                                    'pilihan' => $pilihan[$i],
                                ]);
                            }
                        }
                        $token_hangus = \App\Models\Token::find($id);       
                        $data = [            
                            'status' => '0',
                        ];
                        $token_hangus->update($data);

                        $vote_data = \App\Models\Pilihan::all();
                        // write
                        $file = fopen("backups/pilih.csv","w");
                        foreach ($vote_data as $line) {
                            $pilihan_array = [
                                $line->id,
                                $line->token,
                                $line->id_kegiatan,
                                $line->pilihan,
                                date($line->created_at),
                                date($line->updated_at)
                            ];
                            fputcsv($file, $pilihan_array);
                        }
                        fclose($file);
                        // end write
                        return redirect('/login-token')->with(['success' => 'Data berhasil dikirim']);
                    } catch (\Throwable $th) {
                        return redirect('/login-token/vote-now')->with(['gagal' => 'Ada yang salah. Silahkan ulangi lagi !']);
                    }
                }else{
                    return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Ada yang salah. Silahkan ulangi lagi !']);
                }
            }elseif($date_today == $data_pemilihan and $waktu_today_to_menit >= $waktu_akhir_to_menit){
                return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Pemilihan sudah selesai / belum diatur']);
            }else{
                return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Pemilihan sudah selesai / belum diatur']);
            }
        }else{
            return redirect('/login-token/vote-now/'.$name)->with(['gagal' => 'Voting belum dimulai']);
        }
        
        
    }
}
