<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Calon_FormaturController extends Controller
{
    public function index(Request $request)
    {   
        date_default_timezone_set('Asia/Jakarta');
        $cari = $request->cari;
        if($cari != null){
            $formatur = \App\Models\Formatur::where('name','like',"%".$cari."%")
                ->paginate(100);
        }else{
            $formatur = \App\Models\Formatur::orderby('no_formatur','desc')->paginate(100);
        }
        $daerah = \App\Models\Pimpinan::orderby('name','ASC')->get();
        return view('calon_formatur.index',compact('formatur','daerah'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $duplicate_number = DB::table('formatur as d')->select('d.*')->where('no_formatur',$request->no_formatur)->count();
        
        if( $duplicate_number > 0){
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Gagal ! nomor formatur sama']);
        }
        if( $request->name == null){
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Gagal ! nama calon formatur belum diisi']);
        }
        if( $request->id_daerah == null){
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Gagal ! asal daerah belum diisi']);
        }
        if( $request->status == null){
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Gagal ! status belum diisi']);
        }
        if( $request->no_formatur == null){
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Gagal ! nomor formatur belum diisi']);
        }
        try{
            $formatur = \App\Models\Formatur::create([        
                'name' => $request->name,
                'no_formatur' => $request->no_formatur,
                'id_daerah' =>  $request->id_daerah,
                'status' => $request->status,
                'foto' => $request->foto,
            ]);
            if ($request->hasFile('foto')) {
                $file = $request->file('foto');
                $filename = $request->file('foto')->getClientOriginalName();
                $request->file('foto')->move('img/',$filename);
                $formatur->foto = $request->file('foto')->getClientOriginalName();
                $formatur->save();
            } 
            return redirect('/admin/calon-formatur/')->with(['success' => 'Ipmawan '.$formatur->name.' berhasil ditambahkan']);
        } catch (\Throwable $th) {
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Ipmawan gagal ditambah']);
        }
    }
    public function deactive($id){
        date_default_timezone_set('Asia/Jakarta');
        try{
            $formatur = \App\Models\Formatur::find($id);       
                if($formatur->status == '1'){
                    $data = [            
                        'status' => '0',
                    ];
                    $formatur->update($data);
                    return redirect('/admin/calon-formatur/')->with(['gagal' => 'Ipmawan '.$formatur->name.' di non aktifkan']);
                }elseif($formatur->status == '0'){
                    $data = [            
                        'status' => '1',
                    ];
                    $formatur->update($data);
                    return redirect('/admin/calon-formatur/')->with(['success' => 'Ipmawan '.$formatur->name.' di aktifkan']);
                }
        } catch (\Throwable $th) {
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Status gagal diubah']);
        }
    }
    public function hapus($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $formatur = \App\Models\Formatur::find($id);
            $formatur->delete($formatur);
            return redirect('/admin/calon-formatur/')->with(['success' => 'Ipmawan '.$formatur->name.' berhasil dihapus']);
        } catch (\Throwable $th) {
            return redirect('/admin/calon-formatur/')->with(['gagal' => 'Ipmawan '.$formatur->name.' gagal dihapus']);
        }
    }
    public function edit($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $formatur = \App\Models\Formatur::find($id);
        $daerah = \App\Models\Pimpinan::orderby('name','ASC')->get();
        return view('calon_formatur.edit',compact('formatur','daerah'));
    }
    public function hapus_foto($id,Request $request){
        try{
            $formatur = \App\Models\Formatur::find($id);       
                $data = [            
                    'foto' => null,
                ];
                $formatur->update($data);
            return redirect('/admin/calon-formatur/'.$id)->with(['success' => 'Foto '.$formatur->name.' berhasil dihapus']);
        } catch (\Throwable $th) {
            return redirect('/admin/calon-formatur/'.$id)->with(['gagal' => 'Foto '.$formatur->name.' gagal dihapus']);
        }
    }
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $formatur = \App\Models\Formatur::find($id);       
            $data = [            
                'name' => $request->name,
                'no_formatur' => $request->no_formatur,
                'id_daerah' => $request->id_daerah,
                'status' => $request->status,
            ];
            $formatur->update($data);
            if ($request->hasFile('foto')) {
                $file = $request->file('foto');
                $filename = $request->file('foto')->getClientOriginalName();
                $request->file('foto')->move('img/',$filename);
                $formatur->foto = $request->file('foto')->getClientOriginalName();
                $formatur->save();
            } 
            return redirect('/admin/calon-formatur/')->with(['success' => 'Data '.$formatur->name.' berhasil diperbarui']);
        } catch (\Throwable $th) {
            return redirect('/admin/calon-formatur')->with(['gagal' => 'Data gagal diperbarui']);
        }
    }
}
