<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PengaturanController extends Controller
{
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $cari = $request->cari;
        if($cari != null){
            $pimpinan = \App\Models\Pimpinan::where('name','like',"%".$cari."%")
            ->orderby('name','ASC')->paginate(100);
        }else{
            $pimpinan = \App\Models\Pimpinan::orderby('name','ASC')->paginate(100);
        }
        return view('pengaturan.index',compact('pimpinan'));
    }
    public function daerah_store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        if( $request->name == null ||  $request->jumlah == null){
            return redirect('/admin/pengaturan/')->with(['gagal' => 'Gagal ! data tidak boleh kosong']);
        }
        $pimpinan = \App\Models\Pimpinan::all();
        if(count($pimpinan) != 0){
            for($i=0;$i<count($pimpinan);$i++){
                if($pimpinan[$i]->name == $request->name){
                    return redirect('/admin/pengaturan/')->with(['gagal' => 'Data sudah ada']);
                }
            }
        }
        try{
            $pimpinan = \App\Models\Pimpinan::create([        
                'name' => $request->name,
                'jenis_pimpinan' => "daerah",
                'jumlah' => $request->jumlah,
            ]);
            return redirect('/admin/pengaturan/')->with(['success' => $pimpinan->name.' berhasil ditambahkan']);
        }catch (Exception $e){
            return redirect('/admin/pengaturan/')->with(['gagal' => $pimpinan->name. 'gagal ditambah']);
        }
    }
    public function daerah_edit($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $pimpinan = \App\Models\Pimpinan::find($id);
        return view('pengaturan.edit',compact('pimpinan'));
    }
    public function daerah_update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        if( $request->name == null ||  $request->jumlah == null){
            return redirect('/admin/pengaturan/')->with(['gagal' => 'Gagal ! data tidak boleh kosong']);
        }
        try{
            $pimpinan = \App\Models\Pimpinan::find($id);       
                $data = [            
                    'name' => $request->name,
                    'jumlah' => $request->jumlah,
                ];
                $pimpinan->update($data);
            return redirect('/admin/pengaturan/')->with(['success' => 'Data berhasil diperbarui']);
        }catch (Exception $e){
            return redirect('/admin/pengaturan/')->with(['gagal' => 'Data gagal diperbarui']);
        }
    }
    public function destroy($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $formatur = \App\Models\Formatur::all(); 
            $pimpinan = \App\Models\Pimpinan::find($id);
            for($i=0;$i<count($formatur);$i++){
                if($pimpinan->name == $formatur[$i]->id_daerah){
                    return redirect('/admin/pengaturan/')->with(['gagal' =>  '[TIDAK BISA DIHAPUS] Kota dengan nama '. $pimpinan->name.' masih digunakan di data calon formatur']);
                }
            }
            $pimpinan->delete($pimpinan);
            return redirect('/admin/pengaturan/')->with(['success' => $pimpinan->name. ' berhasil dihapus']);
        }catch (Exception $e){
            return redirect('/admin/pengaturan/')->with(['gagal' =>  $pimpinan->name.' gagal dihapus']);
        }
    }
}
