<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Aplikasi e-Voting PR IPM SMKM 1 Kts. Dibuat untuk keperluan khusus musyawarah tingkat daerah ikatan pelajar muhammadiyah SMKM 1 Kertosono.">
        <meta name="author" content="IPR IPM SMKM 1 Kts">
        <title>e-Vote IPM SMKM 1 Kts</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <!-- Favicon -->
        <link rel="icon" href="{{asset('/assets/img/brand/favicon.png')}}" type="image/png">

        <!-- Additional CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{asset('/assets/landing/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/assets/landing/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/landing/css/templatemo-lava.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/landing/css/owl-carousel.css')}}">
    </head>
    <style>
        /* width */
        ::-webkit-scrollbar {
        width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px dark;
        border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
        background: #fb6340;
        border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
        background: #b30000;
        }
    </style>
    <body>
        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
            </div>
        </div>
        <!-- ***** Preloader End ***** -->
        <!-- ***** Header Area Start ***** -->

        <!-- notif feedback -->
            <?php

                if (Session::get('gagal')){
                    $message = 'gagal';
                }else if (Session::get('success')){
                    $message = 'success';
                }else{
                    $message = '';
                }

            ?>
            <input id="notif" style="display:none" value="{{$message}}"></input>
        <!-- end notif feedback -->
        <header class="header-area header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav class="main-nav">
                            <!-- ***** Logo Start ***** -->
                            <a href="/" class="logo" style="margin-left:0px">
                                e-Vote
                            </a>
                            <!-- ***** Logo End ***** -->
                            <!-- ***** Menu Start ***** -->
                            <ul class="nav">
                                <li class="scroll-to-section"><a href="#welcome" class="menu-item">Beranda</a></li>
                                <li class="scroll-to-section"><a href="#fitur" class="menu-item">Fitur</a></li>
                                {{-- <li class="scroll-to-section"><a href="#panlih" class="menu-item">Panlih</a></li> --}}
                                <li class="scroll-to-section"><a href="/login-token" class="">Voting</a></li>
                                <?php
                                    $akses_hasil = DB::table('history_access as d')->select('d.*')->first();
                                ?>
                                @if($akses_hasil->status_akses == 1)
                                    <li class="scroll-to-section"><a href="/hasil-suara" class="">Hasil Suara</a></li>
                                @endif
                                <li class="scroll-to-section"><a href="#contact-us" class="menu-item">Contact</a></li>
                            </ul>
                            <a class='menu-trigger'>
                                <span>Menu</span>
                            </a>
                            <!-- ***** Menu End ***** -->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- ***** Header Area End ***** -->


        <!-- ***** Welcome Area Start ***** -->
        <div class="welcome-area" id="welcome">

            <!-- ***** Header Text Start ***** -->
            <div class="header-text">
                <div class="container">
                    <div class="row">
                        <div class="left-text col-lg-6 col-md-12 col-sm-12 col-xs-12"
                            data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                            <h1>Aplikasi e-Voting <br><em>PR IPM SMKM 1 Kts</em></h1>
                            <p>Aplikasi voting online untuk pimpinan ranting ikatan pelajar muhammadiyah SMKM1 Kertosono</p>
                            <a href="/login-token" class="main-button-slider">Vote Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ***** Header Text End ***** -->
        </div>
        <!-- ***** Welcome Area End ***** -->

        <!-- ***** Features Big Item Start ***** -->
        <section class="section" id="fitur">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <div class="features-item">
                            <div class="features-icon">
                                <h2>01</h2>
                                <img src="{{asset('/assets/landing/images/features-icon-1.png')}}" alt="">
                                <h4>Realtime Results</h4>
                                <p>Hasil pemilihan langsung tanpa adanya pihak ke 3 dengan aturan sesuai SOP panlih</p>
                                <!-- <a href="#testimonials" class="main-button">
                                    Read More
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                        <div class="features-item">
                            <div class="features-icon">
                                <h2>02</h2>
                                <img src="{{asset('/assets/landing/images/features-icon-2.png')}}" alt="">
                                <h4>Vote Everywhere</h4>
                                <p>Pemilihan bisa dilakukan dimanapun dengan cepat dan akurat <br> (Stable Network)</p>
                                <!-- <a href="#testimonials" class="main-button">
                                    Discover More
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <div class="features-item">
                            <div class="features-icon">
                                <h2>03</h2>
                                <img src="{{asset('/assets/landing/images/features-icon-3.png')}}" alt="">
                                <h4>Realtime Notification</h4>
                                <p>Hasil pengumuman akan langsung diumumkan melalui media sesuai SOP pemilihan</p>
                                <!-- <a href="#testimonials" class="main-button">
                                    More Detail
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Features Big Item End ***** -->

        {{-- <div class="right-image-decor"></div> --}}

        <!-- ***** Testimonials Starts ***** -->
        {{-- <section class="section" id="panlih">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="center-heading">
                            <h2>"Salam <em>Tegak Lurus</em>"</h2>
                            <p>Panitia Pemilihan Musyawarah Wilayah ke-21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-12 col-sm-12 mobile-bottom-fix-big"
                        data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <div class="owl-carousel owl-theme">
                            <div class="item service-item">
                                <div class="author">
                                    <i><img src="/assets/landing/images/testimonial-author-2.png" alt="Author One"></i>
                                </div>
                                <div class="testimonial-content">
                                    <ul class="stars">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <h4>Rifqi Argadianto</h4>
                                    <p>Ketua Panitia Pemilihan MUSYWIL 21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                                    <span>PW IPM Jatim</span>
                                </div>
                            </div>
                            <div class="item service-item">
                                <div class="author">
                                    <i><img src="/assets/landing/images/testimonial-author-1.png" alt="Second Author"></i>
                                </div>
                                <div class="testimonial-content">
                                    <ul class="stars">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <h4>Bima Wicaksono</h4>
                                    <p>Sekertaris Panitia Pemilihan MUSYWIL 21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                                    <span>PW IPM Jatim</span>
                                </div>
                            </div>
                            <div class="item service-item">
                                <div class="author">
                                    <i><img src="/assets/landing/images/testimonial-author-3.png" alt="Author Third"></i>
                                </div>
                                <div class="testimonial-content">
                                    <ul class="stars">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <h4>Sabilulhaq</h4>
                                    <p>Anggota Panitia Pemilihan MUSYWIL 21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                                    <span>PD IPM Kab. Pasuruan</span>
                                </div>
                            </div>
                            <div class="item service-item">
                                <div class="author">
                                    <i><img src="/assets/landing/images/testimonial-author-4.png" alt="Fourth Author"></i>
                                </div>
                                <div class="testimonial-content">
                                    <ul class="stars">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <h4>Abdul Kholis Fadli</h4>
                                    <p>Anggota Panitia Pemilihan MUSYWIL 21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                                    <span>PD IPM Kab. Lamongan</span>
                                </div>
                            </div>
                            <div class="item service-item">
                                <div class="author">
                                    <i><img src="/assets/landing/images/testimonial-author-5.png" alt="Fourth Author"></i>
                                </div>
                                <div class="testimonial-content">
                                    <ul class="stars">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <h4>Bayu Ady Pranoto</h4>
                                    <p>Anggota Panitia Pemilihan MUSYWIL 21 Ikatan Pelajar Muhammadiyah Kabupaten SMKM 1 Kertosono</p>
                                    <span>PD IPM Kota Kediri</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- ***** Testimonials Ends ***** -->

        <!-- ***** Footer Start ***** -->
        <footer id="contact-us">
            <div class="container">
                <div class="footer-content">
                    <div class="row">
                        <!-- ***** Contact Form Start ***** -->
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="contact-form">
                                <form method="POST" id="contact"  action="{{ route('kirim-pesan') }}" >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <fieldset>
                                                <input name="name" class="form-control @error('name') is-invalid @enderror" name="name" type="text" id="name" placeholder="Full Name" required=""
                                                    style="background-color: rgba(250,250,250,0.3);">
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <fieldset>
                                                <input name="email" type="text" id="email" placeholder="E-Mail Address" required="" style="background-color: rgba(250,250,250,0.3);">
                                            </fieldset>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12">
                                            <fieldset>
                                                <textarea name="pesan" rows="6" id="message" placeholder="Your Message"
                                                    required="" style="background-color: rgba(250,250,250,0.3);"></textarea>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-12">
                                            <fieldset>
                                                <button type="submit" id="form-submit" class="main-button feedback-confirm">Kirim Feedback
                                                    Sekarang</button>
                                            </fieldset>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- ***** Contact Form End ***** -->
                        <div class="right-content col-lg-6 col-md-12 col-sm-12">
                            <h2>Tentang e-Vote <br><em>PR IPM SMKM 1 Kts</em></h2>
                            <p>Terimakasih sudah memilih dan percaya dengan kami, semoga apa yang kamu pilih merupakan pilihan terbaik untuk IPM SMKM 1 Kts di masa yang akan datang.
                                <br>Nuun Walqolami Wamaa Yasturuun Wassalamu'alaikum Wr. Wb. <br><br>
                                Punya sesuatu yang ingin disampaikan untuk kami ? <br>Kirimkan feedbackmu melalui email yang terdaftar. Enjoy !</p>
                            {{-- <ul class="social">
                                <li><a target="_blank" href="https://www.facebook.com/ipmjawatimur/"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" href="https://twitter.com/ipmjatim1"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="https://www.instagram.com/ipm_jatim/"><i class="fa fa-instagram"></i></a></li>
                                <li><a target="_blank" href="https://www.youtube.com/channel/UCx-f0CXSRFm27WvnHdFhQAA"><i class="fa fa-youtube"></i></a></li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sub-footer">
                            <p>Copyright &copy; 2021
                                {{-- from
                                <a href="https://ipmjatim.or.id/" class="font-weight-bold ml-1" target="_blank"><b>ipmjatim</b>.or.id</a> --}}
                                <!-- | Designed by <a href="https://www.instagram.com/n_vi25/" class="font-weight-bold ml-1" target="_blank">Media IPM Jatim</a> -->
                                {{-- | Designed by Media IPM Jatim --}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- jQuery -->
        <script src="{{asset('/assets/landing/js/jquery-2.1.0.min.js')}}"></script>

        <!-- Bootstrap -->
        <script src="{{asset('/assets/landing/js/popper.js')}}"></script>
        <script src="{{asset('/assets/landing/js/bootstrap.min.js')}}"></script>

        <!-- Plugins -->
        <script src="{{asset('/assets/landing/js/owl-carousel.js')}}"></script>
        <script src="{{asset('/assets/landing/js/scrollreveal.min.js')}}"></script>
        <script src="{{asset('/assets/landing/js/waypoints.min.js')}}"></script>
        <script src="{{asset('/assets/landing/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('/assets/landing/js/imgfix.min.js')}}"></script>

        <!-- Global Init -->
        <script src="{{asset('/assets/landing/js/custom.js')}}"></script>

        <!-- alert -->
        <script src="{{asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js')}}"></script>
        <script type="text/javascript">
            var notif = document.getElementById('notif').value;
            if(notif == 'success'){
                swal({
                    icon: 'success',
                    title: 'Saran berhasil dikirim',
                    showConfirmButton: false,
                    timer: 5000
                })
            }else if(notif == 'gagal'){
                swal({
                    icon: 'danger',
                    title: 'Saran gagal dikirim',
                    showConfirmButton: false,
                    timer: 5000
                })
            }
        </script>
    </body>
</html>
