<?php

namespace Database\Seeders;

use App\Models\History_Access;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        History_Access::create([
            'status_akses' => 0,
            'max_data_hasil' => 9,
        ]);

        User::create([
            'name' => 'admin1234',
            'email' => 'admin1234@gmail.com',
            'status' => 'admin',
            'password' => Hash::make('admin1234'),
        ]);

        User::create([
            'name' => 'root1234',
            'email' => 'root1234@gmail.com',
            'status' => 'root',
            'password' => Hash::make('root1234'),
        ]);
    }
}
