@extends('layouts.master')

@section('nav-pengaturan')
    active
@endsection

@section('tittle')
    Kelola Pimpinan
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<!-- <div class="header pb-6 d-flex align-items-center" style="min-height: 300px; background-image: url(/assets/img/theme/profile-cover2.jpg); background-size: cover; background-position: center top;"> -->
    <!-- <span class="mask bg-gradient-default opacity-8"></span> -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-12">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kelola Pimpinan</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<style>
    .card{
        margin-bottom:10px;
    }

</style>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Data Pimpinan</h3>
                        </div>
                        <div class="col text-right">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-daerah">+ Tambah</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:60%">Nama Jurusan</th>
                                <th scope="col" style="width:25%">Jumlah Peserta</th>
                                <th scope="col" style="width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($pimpinan as $x)
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>{{ucwords($x->name)}}</td>
                                <td>{{$x->jumlah}}</td>
                                <td>
                                    <a title="Ubah data"  class="btn btn-sm btn-icon btn-warning" href="/admin/pengaturan/daerah_edit/{{$x->id}}">
                                        <span class="btn-inner--icon"><i class="ni ni-settings"></i></span>
                                    </a>

                                    <a title="Hapus Asal Jurusan" class="btn btn-sm btn-icon btn-danger delete-confirm" href="/admin/pengaturan/daerah_hapus/{{$x->id}}">
                                        <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @if(count($pimpinan) == 0)
                                <tr>
                                    <th colspan="4">
                                        <center>Tidak ada data</center>
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation example">
                <div class="row">
                    <div class="col-md-9">
                        @if ($pimpinan->lastPage() > 1)
                            <ul class="pagination">
                                <li class="{{ ($pimpinan->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $pimpinan->url(1) }}" aria-label="Previous">
                                        <i class="fa fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                @for ($i = 1; $i <= $pimpinan->lastPage(); $i++)
                                    <li class="{{ ($pimpinan->currentPage() == $i) ? ' active' : '' }} page-item"><a class="page-link" href="{{ $pimpinan->url($i) }}">{{$i}}</a></li>
                                @endfor
                                <li class="{{ ($pimpinan->currentPage() == $pimpinan->lastPage()) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $pimpinan->url($pimpinan->currentPage()+1) }}" aria-label="Next">
                                        <i class="fa fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <?php
                            $jumlah_entri = $pimpinan->total()/$pimpinan->perPage();
                            $jumlah_entri = intval($jumlah_entri)+1;
                        ?>
                        <p  style="font-size:14px;color:#8f8f8f" align="right">Showing {{ $pimpinan->currentPage() }} to {{$jumlah_entri}} of {{$jumlah_entri}} entries</p>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="modal-daerah" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" action="{{route('daerah_store')}}">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Nama Jurusan</label>
                                    <input type="text" name="name" class="form-control"placeholder="masukkan nama jurusanupaten//kota">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Jumlah Peserta</label>
                                    <input type="number" name="jumlah" class="form-control"placeholder="Masukkan jumlah pimpinan terdata">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal delete-->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <p id="data"></p>
        </div>
        <div class="modal-footer">
            <a class="btn btn-danger confirm" style="color:white" >Hapus sekarang</a>
            <button type="button" class="btn btn-link ml-auto" data-dismiss="modal" style="color:black">Tidak</button>
        </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin dihapus ?',
            text: 'Perhatikan keterkaitan data dengan yang data yang lainya agar susunan data tidak rusak',
            icon: 'warning',
            buttons: ["Batalkan", "Hapus"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection
