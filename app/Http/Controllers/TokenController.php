<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Auth;

class TokenController extends Controller
{
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $cari = $request->cari;
        if($cari != null){
            $token = \App\Models\Token::where('name','like',"%".$cari."%")
                ->orderby('id','desc')->get();
        }else{
            $token = \App\Models\Token::all();
        }
        return view('token.index',compact('token'));
    }
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        if( $request->jumlah == null){
            return redirect('/admin/token')->with(['gagal' => 'Gagal ! jumlah belum diisi']);
        }
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        function generate_string($input, $strength = 16) {
            $input_length = strlen($input);
            $random_string = '';
            for($i = 0; $i < $strength; $i++) {
                $random_character = $input[mt_rand(0, $input_length - 1)];
                $random_string .= $random_character;
            }
        
            return $random_string;
        }
        try{
            for ($i=0;$i<$request->jumlah;$i++){
                $token = \App\Models\Token::create([        
                    'name' => generate_string($permitted_chars, 15),
                    'status' => "1",
                ]);
            }
            
            return redirect('/admin/token')->with(['success' =>  $request->jumlah.' token berhasil dibuat']);
        } catch (\Throwable $th) {
            return redirect('/admin/token')->with(['gagal' =>  $request->jumlah.' token gagal dibuat']);
        }
    }
    public function destroy()
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $all_token = \App\Models\Token::all();
            for ($i=0;$i<count($all_token);$i++){
                $id = $all_token[$i]->id;
                $deleted = \App\Models\Token::find($id);
                $deleted->delete($deleted);
            }
            return redirect('/admin/token/')->with(['success' => 'Semua token berhasil dihapus']);
        } catch (\Throwable $th) {
            return redirect('/admin/token')->with(['gagal' => 'Semua token gagal dihapus']);
        }
    }
    public function status()
    {
        date_default_timezone_set('Asia/Jakarta');
        try{
            $all_token = \App\Models\Token::all();
            for ($i=0;$i<count($all_token);$i++){
                $id = $all_token[$i]->id;
                $token = \App\Models\Token::find($id);
                if($token->status == '0'){
                    $data = [            
                        'status' => 1,
                    ];
                    $massage = "Berhasil di Aktifkan";
                    $token->update($data);
                }else{
                    $data = [            
                        'status' => 0,
                    ];
                    $massage = "Berhasil di Non Aktifkan";
                    $token->update($data);
                }
            }
            return redirect('/admin/token/')->with(['success' => $massage]);
        } catch (\Throwable $th) {
            return redirect('/admin/token')->with(['gagal' => $massage]);
        }
    }
    public function print()
    {
        date_default_timezone_set('Asia/Jakarta');
        $token = \App\Models\Token::orderby('name','ASC')->get();
        $pdf = PDF::loadview('token.cetak_token_pdf',['token'=>$token])->setPaper('A4','potrait');
        return $pdf->stream();
    }
}
