@extends('layouts.master')

@section('nav-duplicate_check')
    active
@endsection

@section('tittle')
    Duplicate Check
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        @php
            $history = \App\Models\History::latest()->first();
            $token_hangus = \App\Models\Token::where('status',0)->count();
            if($history != null){
                $all_suara = \App\Models\Pilihan::where('id_kegiatan',$history->id)->count();
            }
        @endphp
        @if($history != null){
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="fw-bold">Details Check :</h1>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-3">
                                <h3 class="mb-0">
                                    Token Hangus : {{ $token_hangus }} Token
                                </h3>
                            </div>
                            <div class="col-3">
                                <h3 class="mb-0">
                                    Total suara : {{ $all_suara }} Suara
                                </h3>
                            </div>
                            <div class="col-3">
                                <h3 class="mb-0">
                                    Total seharusnya : {{ $token_hangus*9 }} Suara
                                </h3>
                            </div>
                            <div class="col-3">
                                <h3 class="mb-0">
                                    Duplicate : {{ $all_suara - ($token_hangus*9 )}} Suara
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" style="width:5%">No</th>
                                    <th scope="col" >Nama Formatur</th>
                                    <th scope="col" >Total Suara</th>
                                    <th scope="col" >Suara Duplicate</th>
                                    <th scope="col" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $data_hangus = DB::table('token as d')
                                        ->select('d.*')
                                        ->where('status','0')
                                        ->get();
                                @endphp
                                @foreach ($data_hangus as $item => $x)
                                    @php
                                        $pilihan = \App\Models\Pilihan::where('token',$x->name)->count();
                                    @endphp
                                    @if ($pilihan > 9)
                                        <tr>
                                            <th scope="row" style="background: rgb(196, 255, 199)">{{ ++$item }}</th>
                                            <th>{{ $x->name }}</th>
                                            <th>{{ $pilihan }}</th>
                                            <th>{{ $pilihan-9 }}</th>
                                            <th>
                                                <a title="Hapus Data Duplicate" class="btn btn-sm btn-icon btn-danger delete-confirm" href="{{ route('delete_duplicate_check',['id'=>$x->id]) }}">
                                                    <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                                </a>
                                            </th>
                                        </tr>
                                    @endif
                                @endforeach
                                @foreach ($data_hangus as $item => $x)
                                    @php
                                        $pilihan = \App\Models\Pilihan::where('token',$x->name)->count();
                                    @endphp
                                    @if ($pilihan == 9)
                                        <tr>
                                            <th scope="row">{{ ++$item }}</th>
                                            <th>{{ $x->name }}</th>
                                            <th>{{ $pilihan }}</th>
                                            <th>{{ $pilihan-9 }}</th>
                                            <th>
                                                <button class="btn btn-sm btn-icon btn-dark" disabled>
                                                    <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                                </button>
                                            </th>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="text-center">
                            <h3>
                                Tidak ada riwayat pemilihan atau yang sedang berjalan
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection
