@extends('layouts.master')


@section('nav-evoting')
    active
@endsection

@section('tittle')
Finish
@endsection

@section('content')
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>    
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('evoting') }}">Akses Voting</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Hasil Pemilihan Suara</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <button class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-history">+ Buat Voting</button> -->
                    <!-- <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="mb-0">
                      <span class="btn-inner--icon">
                      <i class="ni ni-chart-bar-32" style="margin-right:10px;"></i>
                      </span>Hasil Keseluruhan Suara
                    </h3>
                </div>
                <div class="col text-right">
                    <!-- <a class="btn btn-sm btn-primary" href="/admin/evoting/">
                    <span class="btn-inner--icon">
                        <i class="ni ni-book-bookmark" style="margin-right:10px"></i>
                    </span>
                    Detail</a> -->
                    <a class="btn btn-sm btn-primary" target="_blank" href="/admin/evoting/{{$id}}/hasil/print">
                    <span class="btn-inner--icon">
                        <i class="ni ni-caps-small" style="margin-right:10px"></i>
                    </span>
                    Print</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <script type="text/javascript" src="/assets/chart/Chart.js"></script>
            <canvas id="myChart" style="width: 100vw;height: 40vw"></canvas>
        </div>
        <?php
          
          $hasil_semua = [];
          foreach ($hasil_suara as $key => $value){
            array_push($hasil_semua, $key);
          }
          $id_formatur_all = [];
          for($i=0;$i<count($formatur_all);$i++){
            array_push($id_formatur_all, $formatur_all[$i]->id);
          }
          $cari_id_kosong = array_merge($hasil_semua,$id_formatur_all);
          sort($cari_id_kosong);

          $nilai_unique = [];
          $i=0;
          while($i<count($cari_id_kosong)){
            if($i == count($cari_id_kosong)-1){
              array_push($nilai_unique, $cari_id_kosong[$i]);
              break;
            }
            if($cari_id_kosong[$i]!=$cari_id_kosong[$i+1]){
              array_push($nilai_unique, $cari_id_kosong[$i]);
              $i = $i+1;
            }else{
              $i = $i+2;
            }
          }

          $hasil_semua_key = [];
          foreach ($hasil_suara as $key => $value){
            array_push($hasil_semua_key, [$key,$value]);
          }
          for($i=0;$i<count($nilai_unique);$i++){
            array_push($hasil_semua_key, [$nilai_unique[$i],0]);
          }

          $nilai_max = [];
          foreach ($hasil_suara as $key => $value){
            array_push($nilai_max, $value);
          }

          rsort($nilai_max);

          $id_calon_formatur_terpilih = [];
          for($i=0;$i<9;$i++){
            array_push($id_calon_formatur_terpilih, $nilai_max[$i]);
          }
        ?>
        <div class="card-body">
            <div class="table-responsive">
                <!-- Projects table -->
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width:5%">Foto</th>
                            <th scope="col" style="width:30%">Nama Lengkap</th>
                            <th scope="col" style="width:25%">
                              <center>
                                Total
                              </center>
                            </th>
                            <th scope="col" style="width:40%">
                              <center>
                                Formatur Terpilih
                              </center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($formatur_all as $x)
                        @if($x->status == 1)
                          @foreach ($hasil_semua_key as $id)
                            @if($x->id == $id[0])
                              <?php
                                $user = \App\Models\Formatur::find($x->id);
                              ?>
                              <tr>
                                  <td>
                                      <a href="{{$user->getFoto()}}" target="_blank" tittle="{{$user->foto}}" >
                                          <img src="{{$user->getFoto()}}" width="50px" height="70px" alt="$user->foto">
                                      </a>
                                  </td>
                                  <td>{{ $user->name }} ({{$user->no_formatur}})</td>
                                  <td>
                                    <center>
                                      {{$id[1]}} Suara 
                                    </center>
                                  </td>
                                  <td>
                                    <center>
                                      @if($id[1] == $id_calon_formatur_terpilih[0] or $id[1] == $id_calon_formatur_terpilih[1] or $id[1] == $id_calon_formatur_terpilih[2])
                                        <button class="btn btn-sm btn-success">Terpilih Formatur</button>
                                      @elseif($id[1] == $id_calon_formatur_terpilih[3] or $id[1] == $id_calon_formatur_terpilih[4] or $id[1] == $id_calon_formatur_terpilih[5])
                                        <button class="btn btn-sm btn-success">Terpilih Formatur</button>
                                      @elseif($id[1] == $id_calon_formatur_terpilih[6] or $id[1] == $id_calon_formatur_terpilih[7] or $id[1] == $id_calon_formatur_terpilih[8])
                                        <button class="btn btn-sm btn-success">Terpilih Formatur</button>
                                      @else
                                        <button class="btn btn-sm btn-danger">Tidak Terpilih</button>
                                      @endif
                                    </center>
                                  </td>
                              </tr>
                            @endif
                          @endforeach
                        @endif
                    @endforeach
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('includes.footer')
</div>
<script type="text/javascript">
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: <?php echo json_encode($label); ?>,
      datasets: [{
        label: 'Perolehan suara ',
        data: <?php echo json_encode($suara); ?>,
        backgroundColor: 'rgba(153, 102, 255, 0.2)',
        borderColor: 'rgba(153, 102, 255, 1)',
        
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true
          }
        }]
      }
    }
  });
</script>


<script>
    var elem = document.getElementById("fullscreen");
      function openFullscreen() {
      if (elem.requestFullscreen) {
          elem.requestFullscreen();
      } else if (elem.webkitRequestFullscreen) { /* Safari */
          elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE11 */
          elem.msRequestFullscreen();
      }
    }
</script>
@endsection