<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\Pilihan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RootController extends Controller
{
    public function duplicate_check()
    {
        return view('root.duplicate_check');
    }
    public function delete_duplicate_check($id)
    {
        $token = Token::find($id);
        $pilihan = Pilihan::where('token', $token->name)->get();
        foreach ($pilihan as $index => $x) {
            if ($index >= 9) {
                $delete = Pilihan::find($x->id);
                $delete->delete($delete);
            }
        }
        return redirect()->back()->with(['success', 'Berhasil menghapus data yang duplicate']);
    }
    public function index(Request $request)
    {
        $cari = $request->cari;
        if ($cari != null) {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->where('p.token', $cari)
                ->get();
        } else {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->get();
        }
        $data_kegiatan = \App\Models\History::all();
        $data_formatur = \App\Models\Formatur::all();
        return view('root.index', compact('data_pilihan', 'data_kegiatan', 'data_formatur'));
    }
    public function filter(Request $request)
    {
        $kegiatan = $request->kegiatan;
        $formatur = $request->formatur;
        // dd($formatur);
        if ($formatur != null && $kegiatan != null) {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->where('f.id', $formatur)
                ->where('p.id_kegiatan', $kegiatan)
                ->get();
        } else if ($formatur != null) {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->where('f.id', $formatur)
                ->get();
        } else if ($kegiatan != null) {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->where('p.id_kegiatan', $kegiatan)
                ->get();
        } else {
            $data_pilihan = DB::table('pilihan as p')
                ->select(
                    'p.id',
                    'p.token',
                    'p.id_kegiatan',
                    'p.pilihan',
                    'p.created_at',
                    'h.name as nama_kegiatan',
                    'f.name as nama_formatur',
                    'f.id',
                    'f.id_daerah'
                )
                ->join('history as h', 'h.id', '=', 'p.id_kegiatan')
                ->join('formatur as f', 'f.id', '=', 'p.pilihan')
                ->get();
        }
        $data_kegiatan = \App\Models\History::all();
        $data_formatur = \App\Models\Formatur::all();
        return view('root.index', compact('data_pilihan', 'data_kegiatan', 'data_formatur'));
    }
    public function restore_data()
    {
        // read
        try {
            $pilihanCSV = file('backups/pilih.csv');

            $pilihan = \App\Models\Pilihan::all();
            foreach ($pilihan as $x) {
                $delete = \App\Models\Pilihan::find($x->id);
                $delete->delete($delete);
            }

            foreach ($pilihanCSV as $line) {
                $data = str_getcsv($line);
                // insert to database
                $tambah = \App\Models\Pilihan::create([
                    'id' => $data[0],
                    'token' => $data[1],
                    'id_kegiatan' =>  $data[2],
                    'pilihan' => $data[3],
                    'created_at' => $data[4],
                    'updated_at' => $data[5],
                ]);
            }
            // end read
            return redirect('/admin/root/')->with(['success' => 'Data Berhasil di restore']);
        } catch (\Throwable $th) {
            return redirect('/admin/root/')->with(['gagal' => 'Data gagal di restore']);
        }
    }
    public function role_list(Request $request)
    {
        $data_role = \App\Models\User::all();
        return view('root.role', compact('data_role'));
    }
    public function hapus_role($id)
    {
        try {
            $User = \App\Models\User::find($id);
            $User->delete($User);
            return redirect('/admin/root/role')->with(['success' => 'Berhasil dihapus']);
        } catch (\Throwable $th) {
            return redirect('/admin/root/role')->with(['gagal' => 'Gagal dihapus']);
        }
    }
    public function realtime()
    {
        return view('root.realtime');
    }
}
