@extends('layouts.master-guest')

@section('tittle')
Login Pemilih
@endsection

@section('content')
<style>
    .bg-default{
        background-image: url("../assets/landing/images/bg-login-token.png");
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
</style>
<div class="main-content">
<!-- Header -->
{{-- <div class="header py-7 py-lg-8 pt-lg-9">
    <div class="container">
        <div class="header-body text-center mb-7" style="color: white;font-size: 25px;">
            <div class="row">
                <div class="col-4">
                    <img src="../img/Logo_IPM.png" width="50px" style="float: left">
                </div>
                <div class="col-8">
                    e-Voting <br>
                    <b>IPM SMKM 1 Kts</b>
                </div>
            </div>
        </div>
    </div>

</div> --}}
<div class="header py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
            <img src="../assets/img/brand/white.png" width="200px">
        </div>
      </div>

    </div>
<style>
    .mb-4, .my-4, .form-group {
    margin-bottom: 0rem !important;
    }
    .card-body {
    padding: 0.5rem;
    }
    .mb-7, .my-7 {
    margin-bottom: 3rem !important;
    }
</style>
<!-- Page content -->
<br><br>
<div class="container mt--8 pb-5">
    <!--<div class="row  justify-content-center pb-5">-->
    <!--    <div class="col-lg-4 col-md-6" >-->
    <!--        <div class="row  justify-content-center pb-3">-->
    <!--            <div class="col-4">-->
    <!--                <img src="../img/Logo_IPM.png" width="40px" style="float: right">-->
    <!--            </div>-->
    <!--            <div class="col-8" style="font-size: 20px;color:white">-->
    <!--                e-Voting <br>-->
    <!--                <b>IPM SMKM 1 Kts</b>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <p class="text-center" style="color:white">
        e-Voting
        <b>Login Pemilih</b>
    </p>
<br>
<br>
    <div class="row justify-content-center">
        <div class="col-lg-4 col-md-6" >
            <form method="POST" action="{{ route('login-token') }}"style="margin-bottom:0px">
            @csrf
            @if ($message = Session::get('gagal'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <span class="alert-text">{{$message}}</span>
                </div>
            @elseif ($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    <span class="alert-text">{{$message}}</span>
                </div>
            @endif
            <div class="form-group mb-3" >
                <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-badge"></i></span>
                </div>
                <input id="token" type="text" placeholder="Masukkan token pilih anda" class="form-control @error('token') is-invalid @enderror" name="token" value="{{ old('token') }}" required autocomplete="token" autofocus>
                @error('token')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <button type="submit" style="width:100%;z-index:999999" class="btn btn-primary my-4">
                    <i class="ni ni-key-25" style="margin-right:5px"></i>
                    Masuk Voting
                </button>
            </div>

            </form>
        </div>
    </div>
</div>
</div>
<!-- Footer -->
<footer class="" id="footer-main">
<div class="container">
    @include('includes.footer')
</div>
</footer>
@endsection
