<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function profile()
    {
        return view('profile');
    }   
    // AUTH ============================================
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        if( $request->name == null ||  $request->email == null){
            return redirect('/admin/profile/')->with(['gagal' => 'Gagal ! data tidak boleh kosong']);
        }
        try{
            $user = \App\Models\User::find(Auth::user()->id);       
                $data = [            
                    'name' => $request->name,
                    'email' => $request->email,
                ];
                $user->update($data);
            return redirect('/admin/profile/')->with(['success' => 'Profil berhasil diperbarui']);
        }catch (Exception $e){
            return redirect('/admin/profile/')->with(['gagal' => 'Profil gagal diperbarui']);
        }
    }
    public function ganti_password(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $pass= Hash::check($request->password_lama,Auth::user()->password);
        if ($pass == true){
            if( $request->password == null){
                return redirect('/admin/profile/')->with(['gagal' => 'Gagal ! password tidak boleh kosong']);
            }
            try{
                $user = \App\Models\User::find(Auth::user()->id);       
                    $data = [            
                        'password' => Hash::make($request->password),
                    ];
                    $user->update($data);
                return redirect('/admin/profile/')->with(['success' => 'Password berhasil diperbarui']);
            }catch (Exception $e){
                return redirect('/admin/profile/')->with(['gagal' => 'Password gagal diperbarui']);
            }
        }else{
            return redirect('/admin/profile/')->with(['gagal' => 'Gagal ! password anda salah']);
        }
    }
}
