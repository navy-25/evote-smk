@extends('layouts.master')

@section('nav-calon_formatur')
    active
@endsection

@section('tittle')
    Edit Calon Formatur
@endsection

@section('search')
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-12 col-12">
                    @if ($message = Session::get('gagal'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @elseif ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{$message}}</span>
                        </div>
                    @endif
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="/admin/calon-formatur/">Calon Formatur</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit {{$formatur->name}}</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-sm-12 ">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="/admin/calon-formatur/{{$formatur->id}}/update" enctype="multipart/form-data">
                        @csrf
                        <div class="pl-lg">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Nama Calon Formatur</label>
                                        <input type="text" name="name" value="{{$formatur->name}}" class="form-control"placeholder="Masukkan nama calon formatur">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Nomor Calon Formatur</label>
                                        <input type="number" name="no_formatur" value="{{$formatur->no_formatur}}" class="form-control"placeholder="Masukkan urutan nomor calon formatur">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-8 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Asal Jurusan</label>
                                        <select name="id_daerah"  class="form-control">
                                            <option value="{{$formatur->id_daerah}}">{{$formatur->id_daerah}}</option>
                                            @foreach($daerah as $d)
                                                <option value="{{$d->name}}">{{$d->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Status</label>
                                        <select name="status"  class="form-control">
                                            <?php
                                                $status = $formatur->status;
                                                if($status == "1"){
                                                    $status = 'AKtif';
                                                }else{
                                                    $status = "Non Aktif";
                                                }
                                            ?>
                                            <option value="{{$formatur->status}}">{{$status}}</option>
                                            <option value="1">Aktif</option>
                                            <option value="0">Non Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Pasfoto</label>
                                        <!-- <input type="file" class="form-control"  name="foto">-->
                                        <div class="row">
                                            @if($formatur->foto != null)
                                                <div class="col-md-10 col-sm-8">
                                                    <div class="input-group">
                                                        <input value="{{$formatur->foto}}" type="text" style="margin-bottom:10px" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-12 col-sm-8">
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" style="margin-bottom:10px"  name="foto">
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-2 col-sm-4">
                                                @if($formatur->foto != null)
                                                    <a href="/admin/calon-formatur/{{$formatur->id}}/hapus-foto" style="width:100%;background-color:#de1313;color:white;" class="btn">
                                                        <i class="fa fa-image" style="margin-right:10px"></i>
                                                        Hapus</a>
                                                @endif
                                            </div>
                                        </div>
                                        <small>ukuran pasfoto (3cm x 4cm / 4cm x 6cm)*</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <a  tittle="Kembali" class="btn btn-sm btn-secondary" href="/admin/calon-formatur">Tutup</a>
                                </div>
                                <div class="col-6 text-right">
                                    <span>
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Konfirmasi
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection
