@extends('layouts.master')

@section('nav-home')
    active
@endsection

@section('tittle')
    Beranda Admin
@endsection

@section('search')
@endsection

@section('content')
<style>
    body{
        background:#191c46;
    }
</style>
<!-- Header -->
<div class="header pb-6 d-flex align-items-center" style="min-height: 670px; background-image: url(/assets/img/theme/profile-cover2.jpg); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-7 col-md-10">
                <h1 class="display-2 text-white">Hello Ipmin</h1>
                <p class="text-white mt-0 mb-5">Jangan lupa selalu chek datanya ya, agar proses pemilihan dapat berjalan dengan lancar dan maksimal. Semangat buat Ipmin !</p>
                <a href="{{ route('token') }}" class="btn btn-neutral">
                    <span class="btn-inner--icon">
                        <i class="ni ni-atom" style="margin-right:10px"></i>
                    </span>
                    Generate Token
                </a>
                
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection