@extends('layouts.master')

@section('nav-calon_formatur')
    active
@endsection

@section('tittle')
    Kelola Calon Formatur
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-12">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Calon Formatur</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a> -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Data Calon Formatur</h3>
                        </div>
                        <div class="col text-right">
                            <span>
                                <button title="Tambahkan calon formatur" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-formatur">+ Tambah Calon</button>
                                <button onclick="exportTableToExcel('tblData', 'data calon formatur')" title="Export calon formatur" class="btn btn-sm btn-warning">Export Excel</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush" id="tblData">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:10%">Foto</th>
                                <th scope="col" style="width:30%">Nama Lengkap</th>
                                <th scope="col" style="width:20%">Asal Jurusan</th>
                                <th scope="col" style="width:20%">Status</th>
                                <th scope="col" style="width:15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($formatur as $x)
                                @if($x->status != 'admin')
                                <tr>
                                    <th scope="row">{{$no++}}</th>
                                    <td>
                                        <a href="{{$x->getFoto()}}" target="_blank" tittle="{{$x->foto}}" >
                                            <img src="{{$x->getFoto()}}" width="50px" height="70px" alt="$x->foto">
                                        </a>
                                    </td>
                                    <td>{{$x->name}} ({{$x->no_formatur}})</td>
                                    <td>{{$x->id_daerah}}</td>
                                    <td>
                                        @if($x->status == '1')
                                            <span class="badge badge-dot mr-4">
                                                <i class="bg-success"></i>
                                                <span class="status">Aktif</span>
                                            </span>
                                        @elseif($x->status == '0')
                                            <span class="badge badge-dot mr-4">
                                                <i class="bg-danger"></i>
                                                <span class="status">Non Aktif</span>
                                            </span>
                                        @else
                                            <span class="badge badge-dot mr-4">
                                                <i class="bg-danger"></i>
                                                <span class="status">Unknown</span>
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($x->status == '0')
                                            <a title="Verifikasi" class="btn btn-sm btn-icon btn-success" href="/admin/calon-formatur/{{$x->id}}/ubah-status">
                                                <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                            </a>
                                        @elseif($x->status == '1')
                                            <a title="Verifikasi" class="btn btn-sm btn-icon btn-danger" href="/admin/calon-formatur/{{$x->id}}/ubah-status">
                                                <span class="btn-inner--icon">x</span>
                                            </a>
                                        @endif
                                        <a title="Ubah data"  class="btn btn-sm btn-icon btn-warning" href="/admin/calon-formatur/{{$x->id}}">
                                            <span class="btn-inner--icon"><i class="ni ni-settings"></i></span>
                                        </a>
                                        <a title="Hapus Calon Formatur" class="btn btn-sm btn-icon btn-danger delete-confirm" href="/admin/calon-formatur/hapus-calon/{{$x->id}}">
                                            <span class="btn-inner--icon"><i class="ni ni-basket"></i></span>
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            @if(count($formatur) == 0)
                                <tr>
                                    <th colspan="6">
                                        <center>Tidak ada data</center>
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation example">
                <div class="row">
                    <div class="col-md-9">
                        @if ($formatur->lastPage() > 1)
                            <ul class="pagination">
                                <li class="{{ ($formatur->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $formatur->url(1) }}" aria-label="Previous">
                                        <i class="fa fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                @for ($i = 1; $i <= $formatur->lastPage(); $i++)
                                    <li class="{{ ($formatur->currentPage() == $i) ? ' active' : '' }} page-item"><a class="page-link" href="{{ $formatur->url($i) }}">{{$i}}</a></li>
                                @endfor
                                <li class="{{ ($formatur->currentPage() == $formatur->lastPage()) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $formatur->url($formatur->currentPage()+1) }}" aria-label="Next">
                                        <i class="fa fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <?php
                            $jumlah_entri = $formatur->total()/$formatur->perPage();
                            $jumlah_entri = intval($jumlah_entri)+1;
                        ?>
                        <p  style="font-size:14px;color:#8f8f8f" align="right">Showing {{ $formatur->currentPage() }} to {{$jumlah_entri}} of {{$jumlah_entri}} entries</p>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="modal-formatur" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" action="{{route('formatur-store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Nama Calon Formatur</label>
                                    <input type="text" name="name" class="form-control"placeholder="Masukkan nama calon formatur">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-username">Nomor Calon Formatur</label>
                                    <input type="number" name="no_formatur" class="form-control"placeholder="Masukkan urutan nomor calon formatur">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Asal Jurusan</label>
                                    <select name="id_daerah"  class="form-control">
                                        <option value="">--Pilih Asal Jurusan--</option>
                                        @foreach($daerah as $d)
                                            <option value="{{$d->name}}">{{$d->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Status</label>
                                    <select name="status"  class="form-control">
                                        <option value="">--Pilih Status--</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Non Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Pasfoto</label>
                                    <input type="file" class="form-control"  name="foto">
                                    <small>ukuran pasfoto (3cm x 4cm / 4cm x 6cm)*</small>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                            <div class="col-6 text-right">
                                <span>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Konfirmasi
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<!-- Modal feedback -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Yakin dihapus ?',
            text: 'Perhatikan keterkaitan data dengan yang data yang lainya agar susunan data tidak rusak',
            icon: 'warning',
            buttons: ["Batalkan", "Hapus"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
</script>
@endsection
