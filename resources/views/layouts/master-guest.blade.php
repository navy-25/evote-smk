<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Aplikasi e-Voting PR IPM SMKM 1 Kts. Dibuat untuk keperluan khusus musyawarah tingkat daerah ikatan pelajar muhammadiyah SMKM 1 Kertosono.">
        <meta name="author" content="IPR IPM SMKM 1 Kts">
        <title>e-Vote IPM SMKM 1 Kts | @yield('tittle')</title>
        <!-- Favicon -->
        <link rel="icon" href="/assets/img/brand/favicon.png" type="image/png">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <!-- Icons -->
        <link rel="stylesheet" href="/assets/vendor/nucleo/css/nucleo.css" type="text/css">
        <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
        <!-- Argon CSS -->
        <link rel="stylesheet" href="/assets/css/argon.css?v=1.2.0" type="text/css">
    </head>
    <body class="bg-default">
        <style>
            /* width */
            ::-webkit-scrollbar {
            width: 10px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px dark;
            border-radius: 10px;
            }

            /* Handle */
            ::-webkit-scrollbar-thumb {
            background: #fb6340;
            border-radius: 10px;
            }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
            background: #b30000;
            }
        </style>
        @yield('content')
        <!-- Argon Scripts -->
        <!-- Core -->

        <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/vendor/js-cookie/js.cookie.js"></script>
        <script src="/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
        <!-- Argon JS -->
        <script src="/assets/js/argon.js?v=1.2.0"></script>
    </body>
</html>
