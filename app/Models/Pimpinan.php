<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pimpinan extends Model
{
    use HasFactory;
    protected $table = 'pimpinan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'id_daerah',
        'jenis_pimpinan',
        'jumlah',
    ];
}
