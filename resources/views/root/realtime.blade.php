@extends('layouts.master')

@section('nav-realtime')
    active
@endsection

@section('tittle')
    Realtime Result
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        @php
            $history = \App\Models\History::latest()->first();
            if($history != null){
                $all_suara = \App\Models\Pilihan::where('id_kegiatan',$history->id)->count();
            }
        @endphp
        @if($history != null){
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <h3 class="mb-0">
                                    Realtime Suara (Total : {{ $all_suara }} Suara)
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" style="width:5%">No</th>
                                    <th scope="col" >Nama Formatur</th>
                                    <th scope="col" >Total Suara</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $data = DB::table('formatur as d')
                                        ->select('d.*')
                                        ->orderBy('no_formatur','ASC')
                                        ->get();
                                    foreach ($data as $key => $x) {
                                        $total_suara = \App\Models\Pilihan::where('pilihan',$x->id)->where('id_kegiatan',$history->id)->count();
                                        $data_suara[] = [
                                            'total_suara' => $total_suara,
                                            'id_formatur' => $x->id,
                                        ];
                                    }
                                    rsort($data_suara);
                                @endphp
                                @foreach ($data_suara as $item => $x)
                                    @php
                                        $formatur = \App\Models\Formatur::find($x['id_formatur']);
                                    @endphp
                                    <tr>
                                        <th scope="row" style="{{ $item < 9 ? "background: rgb(196, 255, 199)" : ""}}">{{ ++$item }}</th>
                                        <th>{{ $formatur->name }} ({{ $formatur->no_formatur }})</th>
                                        <th>{{ $x['total_suara']  }} Suara</th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="text-center">
                            <h3>
                                Tidak ada riwayat pemilihan atau yang sedang berjalan
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>
@endsection
