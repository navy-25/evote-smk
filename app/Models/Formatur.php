<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formatur extends Model
{
    use HasFactory;
    protected $table = 'formatur';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'no_formatur',
        'id_daerah',
        'foto',
        'status',
    ];
    public function getFoto(){
        if($this->foto != null){
            return asset('img/'.$this->foto);
        }else{
            return asset('img/no-image.png'.$this->foto);
        }
    }
}
