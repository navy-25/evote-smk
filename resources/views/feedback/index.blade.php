@extends('layouts.master')

@section('nav-feedback')
    active
@endsection

@section('tittle')
    Feedback
@endsection

@section('search')
<form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" name="cari" placeholder="Search" type="text">
        </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</form>
@endsection

@section('content')
<!-- Header -->
<div class="header bg-gradient-default pb-6 opacity-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-2">
                <div class="col-lg-12 col-12">
                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <!-- <span class="alert-icon"><i class="ni ni-like-2"></i></span> -->
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @elseif ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">{{$message}}</span>
                    </div>
                @endif
                </div>    
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0"></h6>
                    <style>
                        .ml-md-4, .mx-md-4 {
                            margin-left: 0 !important;
                        }
                    </style>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Feedback</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <!-- <a href="{{route('token-cetak')}}" target="_blank" title="Cetak token" class="btn btn-sm btn-neutral"><i class="ni ni-folder-17" style="margin-right:7px"></i> Print</a> -->
                </div>
            </div>
           
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">
                                Feedback
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width:5%">No</th>
                                <th scope="col" style="width:25%">Pengirim</th>
                                <th scope="col" style="width:45%">Email</th>
                                <th scope="col" style="width:25%">Tanggal</th>
                                <th scope="col" style="width:25%">Pesan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($feedback as $x)
                            <tr>
                                <th scope="row">{{$no++}}</th>
                                <td>{{$x->name}}</td>
                                <td>{{$x->email}}</td>
                                <td>{{$x->updated_at}}</td>
                                <td>
                                    <button  title="Lihat pesan"  class="btn btn-sm btn-icon btn-info passingID" 
                                        data-name="{{$x->name}}" data-email="{{$x->email}}"  data-pesan="{{$x->pesan}}"  
                                        data-toggle="modal" data-target="#modalShow">
                                        <span class="btn-inner--icon"><i class="ni ni-chat-round"></i></span>
                                    </button >
                                </td>
                            </tr>
                            @endforeach
                            @if(count($feedback) == 0)
                                <tr>
                                    <th colspan="5">
                                        <center>Tidak ada data</center> 
                                    </th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation example">
                <div class="row">
                    <div class="col-md-9">
                        @if ($feedback->lastPage() > 1)
                            <ul class="pagination">
                                <li class="{{ ($feedback->currentPage() == 1) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $feedback->url(1) }}" aria-label="Previous">
                                        <i class="fa fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                @for ($i = 1; $i <= $feedback->lastPage(); $i++)
                                    <li class="{{ ($feedback->currentPage() == $i) ? ' active' : '' }} page-item"><a class="page-link" href="{{ $feedback->url($i) }}">{{$i}}</a></li>
                                @endfor
                                <li class="{{ ($feedback->currentPage() == $feedback->lastPage()) ? ' disabled' : '' }} page-item">
                                    <a class="page-link" href="{{ $feedback->url($feedback->currentPage()+1) }}" aria-label="Next">
                                        <i class="fa fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <?php
                            $jumlah_entri = $feedback->total()/$feedback->perPage();
                            $jumlah_entri = intval($jumlah_entri)+1;
                        ?>
                        <p  style="font-size:14px;color:#8f8f8f" align="right">Showing {{ $feedback->currentPage() }} to {{$jumlah_entri}} of {{$jumlah_entri}} entries</p>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Footer -->
    @include('includes.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      </style>
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-title" id="hasil-name" ></div>
            <div class="modal-title" id="hasil-email" ></div>
            <div class="modal-title" style="margin-top:20px" id="hasil-pesan" ></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<!-- Modal feedback -->
<script>
    var hasil_name = document.getElementById("hasil-name");
    var hasil_email = document.getElementById("hasil-email");
    var hasil_pesan = document.getElementById("hasil-pesan");
    $(".passingID").click(function () {
        var name = $(this).attr('data-name');
        var email = $(this).attr('data-email');
        var pesan = $(this).attr('data-pesan');
        hasil_name.innerHTML = "From  : "+name;
        hasil_email.innerHTML = "Email : "+email;
        hasil_pesan.innerHTML = pesan;
        $('#myModal').modal('show');
    });
</script>
@endsection